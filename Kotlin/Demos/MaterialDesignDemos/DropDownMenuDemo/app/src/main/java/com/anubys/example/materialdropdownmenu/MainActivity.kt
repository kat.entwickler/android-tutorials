/**
 *  @Author Created by Anubys on 17.04.2021
 */

package com.anubys.example.materialdropdownmenu

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import com.anubys.example.materialdropdownmenu.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private val tag = MainActivity::class.java.simpleName

    private lateinit var binding: ActivityMainBinding


    //* ************************************************ *
    //*               L I F E - C Y C L E                *
    //* ************************************************ *
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(tag,"TAG - MainActivity - onCreate()")
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setAdapter()
    }


    //* ************************************************ *
    //*         H E L P E R  -  M E T H O D S            *
    //* ************************************************ *
    private fun setAdapter() {
        Log.d(tag,"TAG - MainActivity - setAdapter()")

        val language = resources.getStringArray(R.array.languages)
        val arrayAdapter = ArrayAdapter(applicationContext, R.layout.layout_item_dropdown, language)
        binding.tvTitle.setAdapter(arrayAdapter)
    }
}
