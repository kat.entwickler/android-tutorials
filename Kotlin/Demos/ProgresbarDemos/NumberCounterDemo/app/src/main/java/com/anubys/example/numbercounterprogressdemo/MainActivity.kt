package com.anubys.example.numbercounterprogressdemo

/** @Author Created by Anubys on 09.08.2020
 *          Updated by Anubys on 04.11.2021
 */

import android.animation.ValueAnimator
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.anubys.example.numbercounterprogressdemo.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private val tag = MainActivity::class.java.simpleName

    private lateinit var binding: ActivityMainBinding


    //* ************************************************ *
    //*               L I F E - C Y C L E                *
    //* ************************************************ *
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(tag,"TAG - MainActivity - onCreate()")
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        startAnimationCounter(0, 100)
    }


    //**************************************************
    //*         H E L P E R  -  M E T H O D S          *
    //**************************************************
    private fun startAnimationCounter(start: Int, end: Int) {
        Log.d(tag,"TAG - MainActivity - startAnimationCounter()")
        val valueAnimator: ValueAnimator = ValueAnimator.ofInt(start, end)
        valueAnimator.duration = 5000
        /*
        valueAnimator.addUpdateListener { animation ->
            binding?.tvCounter?.text = animation?.animatedValue.toString()
            binding?.progressbar?.progress = animation?.animatedValue.toString().toInt()
        }
        */
        valueAnimator.addUpdateListener ( OnAnimationUpdateListener() )
        valueAnimator.start()
    }


    //* ************************************************ *
    //*                 C L A S S E S                    *
    //* ************************************************ *
    private inner class OnAnimationUpdateListener : ValueAnimator.AnimatorUpdateListener {
        private val tag = OnAnimationUpdateListener::class.java.simpleName

        override fun onAnimationUpdate(animation: ValueAnimator?) {
            Log.d(tag,"TAG - OnAnimationUpdateListener - onAnimationUpdate()")

            binding.apply {
                tvCounter.text = animation?.animatedValue.toString()
                binding.progressbar.progress = animation?.animatedValue.toString().toInt()
            }
        }
    }
}
