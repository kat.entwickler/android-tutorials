package com.anubys.example.sharedpreferencesdemo

/** @Author Created by Anubys on 16.05.2020 */

import android.app.Application
import android.util.Log

val sharedPreferences: AppPreference by lazy {
    BaseApplication.appPreference!!
}

class BaseApplication : Application() {
    private val tag = BaseApplication::class.java.simpleName

    companion object {
        var appPreference: AppPreference? = null
    }

    override fun onCreate() {
        Log.v(tag,"TAG - BaseApplication - onCreate()")
        super.onCreate()

        appPreference = AppPreference(this)
    }
}
