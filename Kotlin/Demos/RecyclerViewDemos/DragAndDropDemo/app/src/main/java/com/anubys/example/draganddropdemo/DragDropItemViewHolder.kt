/**
 * @Author Created by Anubys on 04.12.2020
 *         Updated by Anubys on 17.04.2021
 */

package com.anubys.example.draganddropdemo

import android.annotation.SuppressLint
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.anubys.example.draganddropdemo.databinding.LayoutDragDropItemBinding
import com.anubys.example.draganddropdemo.reposotory.interfaces.OnStartDragListener
import com.anubys.example.draganddropdemo.reposotory.model.Item


class DragDropItemViewHolder(private val binding: LayoutDragDropItemBinding) : RecyclerView.ViewHolder(binding.root) {
    private val tag = DragDropItemViewHolder::class.java.simpleName


    //* ************************************************ *
    //*             I T E M  E L E M E N T S             *
    //* ************************************************ *
    fun bind(item: Item) {
        Log.d(tag,"TAG - DragDropItemViewHolder - bind()")

        binding.tvTitle.text = item.title
    }

    @SuppressLint("ClickableViewAccessibility")
    fun actionListener(startDragListener: OnStartDragListener) {

        binding.ivIcon.setOnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_DOWN) {
                startDragListener.onStartDrag(this)
                v.performClick()
            }

            return@setOnTouchListener true
        }
    }

    companion object {
        fun create(parent: ViewGroup): DragDropItemViewHolder {
            val binding = LayoutDragDropItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )

            return DragDropItemViewHolder(binding)
        }
    }
}
