/**
 *  @Author Created by Anubys on 04.12.2020
 *          Updated by Anubys on 17.04.2021
 */

package com.anubys.example.draganddropdemo.reposotory.model

import androidx.annotation.DrawableRes
import com.anubys.example.draganddropdemo.R


data class Item(val title: String, @DrawableRes val icon: Int = R.drawable.ic_launcher_background)
