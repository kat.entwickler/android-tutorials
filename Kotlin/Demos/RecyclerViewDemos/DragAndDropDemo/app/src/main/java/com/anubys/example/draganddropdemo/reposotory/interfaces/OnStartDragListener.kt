/**
 * @Author Created by Anubys on 04.12.2020
 *         Updated by Anubys on 17.04.2021
 */

package com.anubys.example.draganddropdemo.reposotory.interfaces

import androidx.recyclerview.widget.RecyclerView


interface OnStartDragListener {
    fun onStartDrag(viewHolder: RecyclerView.ViewHolder)
}
