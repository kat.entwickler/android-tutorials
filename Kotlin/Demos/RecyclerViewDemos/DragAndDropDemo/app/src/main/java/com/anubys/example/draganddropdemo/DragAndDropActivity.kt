/**
 *  @Author Created by Anubys on 04.12.2020
 *          Updated by Anubys on 17.04.2021
 */

package com.anubys.example.draganddropdemo

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.anubys.example.draganddropdemo.databinding.ActivityMainBinding
import com.anubys.example.draganddropdemo.reposotory.interfaces.OnStartDragListener
import com.anubys.example.draganddropdemo.reposotory.model.Item


class DragAndDropActivity : AppCompatActivity(), OnStartDragListener {
    private val tag = DragAndDropActivity::class.java.simpleName

    private lateinit var itemTouchHelper: ItemTouchHelper
    private lateinit var binding: ActivityMainBinding

    private val itemAdapter by lazy {
        DragDropRecyclerViewAdapter(this)
    }

    private val listOfSeasons = listOf(
        Item("Januar"),
        Item("Februar"),
        Item("März"),
        Item("April"),
        Item("Mai"),
        Item("Juni"),
        Item("Juli"),
        Item("August"),
        Item("September"),
        Item("Oktober"),
        Item("November"),
        Item("Dezember")
    )


    //* ************************************************ *
    //*               L I F E - C Y C L E                *
    //* ************************************************ *
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d(tag,"TAG - DragAndDropActivity - onCreate()")
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initAdapter()
        initTouchHelper()
    }

    override fun onStartDrag(viewHolder: RecyclerView.ViewHolder) {
        Log.d(tag,"TAG - DragAndDropActivity - onStartDrag()")

        itemTouchHelper.startDrag(viewHolder)
    }


    //* ************************************************ *
    //*         H E L P E R  -  M E T H O D S            *
    //* ************************************************ *
    private fun initAdapter() {
        Log.d(tag,"TAG - DragAndDropActivity - initAdapter()")

        itemAdapter.setItem(listOfSeasons)
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = itemAdapter
    }

    private fun initTouchHelper() {
        Log.d(tag,"TAG - DragAndDropActivity - initTouchHelper()")

        val itemTouchHelperCallback: ItemTouchHelper.Callback = ItemTouchHelperCallback(itemAdapter)
        itemTouchHelper = ItemTouchHelper(itemTouchHelperCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerView)
    }
}
