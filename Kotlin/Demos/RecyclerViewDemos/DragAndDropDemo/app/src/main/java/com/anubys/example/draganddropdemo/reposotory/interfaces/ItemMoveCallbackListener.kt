/**
 *  @Author Created by Anubys on 04.12.2020
 *          Updated by Anubys on 17.04.2021
 */

package com.anubys.example.draganddropdemo.reposotory.interfaces

import com.anubys.example.draganddropdemo.DragDropItemViewHolder


interface ItemMoveCallbackListener {
    fun onRowMoved(fromPosition: Int, toPosition: Int)
    fun onRowSelected(itemViewHolder: DragDropItemViewHolder)
    fun onRowClear(itemViewHolder: DragDropItemViewHolder)
}
