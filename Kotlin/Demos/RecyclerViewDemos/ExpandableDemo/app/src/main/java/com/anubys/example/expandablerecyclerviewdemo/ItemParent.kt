package com.anubys.example.expandablerecyclerviewdemo

/** @Author Created by Anubys on 04.12.2020 */

data class ItemParent(var parentName: String, var childDataItems: ArrayList<DummyChildDataItem>)
