package com.anubys.example.carouselrecyclerviewdemo

/** @Author Created by Anubys on 04.12.2020 */

import androidx.annotation.DrawableRes


data class Item(val title: String, @DrawableRes val icon: Int)
