package com.anubys.exercises.vocabularytrainerapp.main

/** @Author Created by Anubys on 22.11.2020 */

import android.app.Application

import androidx.lifecycle.ViewModelProvider


class FirstViewModelFactory(application: Application): ViewModelProvider.AndroidViewModelFactory(application)
