package com.anubys.exercises.vocabularytrainerapp.interfaces

/** @Author Created by Anubys on 22.11.2020 */


interface OnItemLongClickListener {
    fun setOnItemLongClickListener(position: Int)
}
