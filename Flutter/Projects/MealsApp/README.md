# Shop App

### Aufgabe:
Schreiben Sie eine App namens ShopApp.

Die App soll aus mehreren Screens bestehen. Es soll den Screen Auth, Cart, Edit, Orders, ProductDetail, ProductsOverview, Splash und UserProducts geben.
Des Weiteren soll ein Menü (Drawer und BottomNavigation) implementiert werden. Sobald eine Kategorie angeklickt wird, soll eine weitere mit mehr Informationen
ListView angezeigt werden. In der ListView sind dann verschiedene Gerichte drin. Per Anklicken soll das Gericht die Detail anzeigen.


#### Ziele
- Verschiedene Widges verwenden (Scaffold, TextButton, ListView, etc.)
- Navigation zwischen den Screens
- Die Möglichkeiten von anklickbaren Objekten
- Models anlegen
- Umgang mit Maps
- Eine DummyData Datei einlegen mit Werten
- Fonts einbinden und Image
- Menu erstellen (AppBar, TabBar, BottomNavigationBar, Drawer)
- Entfernen von Items aus der Liste
- Filtern nach bestimmten Kriterien
- Items ins Favorite einfügen
