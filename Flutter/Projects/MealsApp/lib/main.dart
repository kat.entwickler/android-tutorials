import 'package:flutter/material.dart';
import 'package:meals_app/presentation/category/category_meals_screen.dart';
import 'package:meals_app/core/dummy_data.dart';
import 'package:meals_app/presentation/filter/filters_screen.dart';
import 'package:meals_app/presentation/meal/meal_detail_screen.dart';
import 'package:meals_app/presentation/core/tabs_screen.dart';
import 'core/constants.dart';
import 'domain/entities/meal.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegetarian': false,
  };
  List<Meal> _availableMeals = dummyMeals;
  final List<Meal> _favoriteMeals = [];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        canvasColor: const Color.fromRGBO(255, 254, 229, 1),
        fontFamily: "Raleway",
        colorScheme:
            ThemeData.light().colorScheme.copyWith(secondary: Colors.amber),
        textTheme: ThemeData.light().textTheme.copyWith(
              subtitle1: const TextStyle(
                fontSize: 24,
                fontFamily: "RobotoCondensed",
                fontWeight: FontWeight.bold,
              ),
              bodyText1: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
              bodyText2: const TextStyle(
                color: Color.fromRGBO(20, 51, 51, 1),
              ),
            ),
      ),
      routes: {
        Routes.home: (context) => TabsScreen(favoriteMeals: _favoriteMeals),
        Routes.categoryMeals: (context) =>
            CategoryMealsScreen(availableMeals: _availableMeals),
        Routes.mealDetail: (context) => MealDetailScreen(
              toggleFavorite: _toggleFavorite,
              isFavorite: isMealFavorite,
            ),
        Routes.filters: (context) =>
            FiltersScreen(currentFilter: _filters, saveFilters: _setFilters),
      },
      // Sorgt dafür, dass wenn eine route nicht exsistiert, dann zur CategoriesScreen weitergeleitet wird
      onGenerateRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) => TabsScreen(favoriteMeals: _favoriteMeals),
        );
      },
      // Man gelangt auf die CategoriesScreen, falls man etwas nicht bedacht hat und die App abstützen würde.
      onUnknownRoute: (settings) {
        return MaterialPageRoute(
          builder: (context) => TabsScreen(favoriteMeals: _favoriteMeals),
        );
      },
    );
  }

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;

      _availableMeals = dummyMeals.where((meal) {
        if (_filters['gluten'] == true && !meal.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] == true && !meal.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] == true && !meal.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] == true && !meal.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavorite(String mealId) {
    final existingIndex =
        _favoriteMeals.indexWhere((meal) => meal.id == mealId);

    if (existingIndex >= 0) {
      setState(() {
        _favoriteMeals.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoriteMeals.add(dummyMeals.firstWhere((meal) => meal.id == mealId));
      });
    }
  }

  bool isMealFavorite(String id) {
    return _favoriteMeals.any((meal) => meal.id == id);
  }
}
