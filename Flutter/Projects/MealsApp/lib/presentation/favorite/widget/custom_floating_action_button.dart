import 'package:flutter/material.dart';

class CustomFloatingActionButton extends StatelessWidget {
  final Icon icon;
  final Function onPressed;

  const CustomFloatingActionButton({
    Key? key,
    required this.icon,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      // Notwendig, wenn mehrere erstellt werden auf dem gleichen Widget
      heroTag: null,
      child: icon,
      onPressed: () => onPressed(),
    );
  }
}
