import 'package:flutter/material.dart';
import 'package:meals_app/presentation/core/core_listview.dart';

import '../../domain/entities/meal.dart';

class FavoritesScreen extends StatefulWidget {
  final List<Meal> favoriteMeals;
  final Function removeItem;

  const FavoritesScreen({
    Key? key,
    required this.favoriteMeals,
    required this.removeItem,
  }) : super(key: key);

  @override
  State<FavoritesScreen> createState() => _FavoritesScreenState();
}

class _FavoritesScreenState extends State<FavoritesScreen> {
  @override
  Widget build(BuildContext context) {
    if (widget.favoriteMeals.isEmpty) {
      return const Center(
        child: Text("The Favorites is empty!"),
      );
    } else {
      return CoreListView(
        mealsList: widget.favoriteMeals,
        removeMeal: _removeMeal,
        isDetailScreen: false,
      );
    }
  }

  void _removeMeal(String mealId) {
    setState(() {
      widget.favoriteMeals.removeWhere((meal) => meal.id == mealId);
    });
  }
}
