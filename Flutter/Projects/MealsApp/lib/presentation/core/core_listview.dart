import 'package:flutter/material.dart';
import 'package:meals_app/domain/entities/meal.dart';
import 'package:meals_app/presentation/meal/meal_item.dart';

class CoreListView extends StatelessWidget {
  final List<Meal> mealsList;
  final Function removeMeal;
  final bool isDetailScreen;

  const CoreListView({
    Key? key,
    required this.mealsList,
    required this.removeMeal,
    required this.isDetailScreen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: mealsList.length,
      itemBuilder: (context, index) {
        return MealItem(
          id: mealsList[index].id,
          title: mealsList[index].title,
          imageUrl: mealsList[index].imageUrl,
          duration: mealsList[index].duration,
          complexity: mealsList[index].complexity,
          affordability: mealsList[index].affordability,
          removeItem: removeMeal,
          isDetailScreen: isDetailScreen,
        );
      },
    );
  }
}
