import 'package:flutter/material.dart';
import 'package:meals_app/presentation/category/category_screen.dart';
import 'package:meals_app/presentation/favorite/favorites_screen.dart';
import 'package:meals_app/presentation/core/main_drawer.dart';

import '../../domain/entities/meal.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favoriteMeals;

  const TabsScreen({Key? key, required this.favoriteMeals}) : super(key: key);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  int _selectPageIndex = 0;
  late List<Map<String, dynamic>> _pages;

  @override
  void initState() {
    super.initState();

    _pages = [
      {
        'title': 'Categories',
        'page': const CategoryScreen(),
      },
      {
        'title': 'Your Favorites',
        'page': FavoritesScreen(
          favoriteMeals: widget.favoriteMeals,
          removeItem: () {},
        ),
      },
    ];
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(_pages[_selectPageIndex]['title']),
      ),
      body: _pages[_selectPageIndex]['page'],
      bottomNavigationBar: BottomNavigationBar(
        onTap: _selectPage,
        unselectedItemColor: Colors.white,
        selectedItemColor: Theme.of(context).colorScheme.secondary,
        currentIndex: _selectPageIndex,
        backgroundColor: Theme.of(context).primaryColor,
        type: BottomNavigationBarType.shifting,
        items: [
          BottomNavigationBarItem(
            icon: const Icon(Icons.category),
            backgroundColor: Theme.of(context).primaryColor,
            label: "Categories",
          ),
          BottomNavigationBarItem(
            icon: const Icon(Icons.favorite),
            backgroundColor: Theme.of(context).primaryColor,
            label: "Favorite",
          ),
        ],
      ),
      drawer: const MainDrawer(),
    );
  }

  void _selectPage(int index) {
    setState(() {
      _selectPageIndex = index;
    });
  }
}
