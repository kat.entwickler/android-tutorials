import 'package:flutter/material.dart';
import 'package:meals_app/domain/entities/meal.dart';

import '../../core/constants.dart';

class MealItem extends StatelessWidget {
  final String id;
  final String title;
  final String imageUrl;
  final int duration;
  final Complexity complexity;
  final Affordability affordability;
  final Function removeItem;
  final bool isDetailScreen;

  const MealItem({
    Key? key,
    required this.id,
    required this.title,
    required this.imageUrl,
    required this.duration,
    required this.complexity,
    required this.affordability,
    required this.removeItem,
    required this.isDetailScreen,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectMeal(context),
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        elevation: 4,
        margin: const EdgeInsets.all(10),
        child: Column(
          children: [
            Stack(
              children: [
                ClipRRect(
                  // Oben links/rechts einen Radius hinzufügen
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: Image.network(
                    imageUrl,
                    height: 250,
                    width: double.infinity,
                    // Sorgt dafür, dass es immer in die Box passt
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                    width: 220,
                    color: Colors.black54,
                    padding: const EdgeInsets.symmetric(
                      vertical: 5,
                      horizontal: 20,
                    ),
                    child: Text(
                      title,
                      style: const TextStyle(
                        fontSize: 26,
                        color: Colors.white,
                      ),
                      softWrap: true,
                      // Sollte der Text über den Rand gehen, wird der Text durchsichtig
                      overflow: TextOverflow.fade,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Row(
                    children: [
                      const Icon(Icons.schedule),
                      const SizedBox(width: 6),
                      Text("$duration min"),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(Icons.work),
                      const SizedBox(width: 6),
                      Text(complexityText),
                    ],
                  ),
                  Row(
                    children: [
                      const Icon(Icons.attach_money),
                      const SizedBox(width: 6),
                      Text(affordabilityText),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void selectMeal(BuildContext context) {
    Navigator.of(context).pushNamed(
      Routes.mealDetail,
      arguments: {
        'id': id,
        'isDetailScreen': isDetailScreen,
      },
    )
        // Wird erst ausgeführt, wenn man aus Details zurückkehrt. Reagiert auf .pop()
        .then((result) => removeItem(result));
  }

  String get complexityText {
    switch (complexity) {
      case Complexity.SIMPLE:
        return "Simple";
      case Complexity.CHALLENGING:
        return "Challenging";
      case Complexity.HARD:
        return "Hard";
      default:
        return "Unknown";
    }

    // Alternative Möglichkeit
/*    if(complexity == Complexity.SIMPLE) {
      return "Simple";
    }

    if(complexity == Complexity.CHALLENGING) {
      return "Challenging";
    }

    if(complexity == Complexity.HARD) {
      return "Hard";
    }

    return "";*/
  }

  String get affordabilityText {
    switch (affordability) {
      case Affordability.AFFORDABLE:
        return "Affordable";
      case Affordability.PRICEY:
        return "Pricey";
      case Affordability.LUXURIOUS:
        return "Luxurious";
      default:
        return "Unknown";
    }
  }
}
