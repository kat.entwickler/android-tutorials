import 'package:flutter/material.dart';
import 'package:meals_app/core/dummy_data.dart';
import 'package:meals_app/presentation/favorite/widget/custom_container.dart';
import 'package:meals_app/presentation/favorite/widget/custom_floating_action_button.dart';
import 'package:meals_app/presentation/favorite/widget/section_title.dart';

class MealDetailScreen extends StatelessWidget {
  final Function toggleFavorite;
  final Function isFavorite;

  const MealDetailScreen({
    Key? key,
    required this.toggleFavorite,
    required this.isFavorite,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final routeArgs = ModalRoute.of(context)?.settings.arguments as Map<String, dynamic>;
    final mealId = routeArgs['id'];
    final isDetailScreen = routeArgs['isDetailScreen'];
    final selectedMeal = dummyMeals.firstWhere((meal) => meal.id == mealId);

    return Scaffold(
      appBar: AppBar(
        title: Text(selectedMeal.title),
      ),
      body: SingleChildScrollView(
        child: Column(children: [
          SizedBox(
            height: 300,
            width: double.infinity,
            child: Image.network(
              selectedMeal.imageUrl,
              fit: BoxFit.cover,
            ),
          ),
          const SectionTitle(title: "Ingredients"),
          CustomContainer(
            child: ListView.builder(
              itemCount: selectedMeal.ingredients.length,
              itemBuilder: (context, index) => Card(
                color: Theme.of(context).colorScheme.secondary,
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                    vertical: 5,
                    horizontal: 10,
                  ),
                  child: Text(selectedMeal.ingredients[index]),
                ),
              ),
            ),
          ),
          const SectionTitle(title: "Steps"),
          CustomContainer(
            child: ListView.builder(
              itemCount: selectedMeal.steps.length,
              itemBuilder: (context, index) => Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      child: Text("# ${index + 1}"),
                    ),
                    title: Text(
                      selectedMeal.steps[index],
                      style: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const Divider(),
                ],
              ),
            ),
          ),
        ]),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          if (isDetailScreen)
            CustomFloatingActionButton(
              icon: const Icon(Icons.delete),
              onPressed: () => Navigator.of(context).pop(mealId),
            ),
          const SizedBox(height: 10),
          CustomFloatingActionButton(
            icon: Icon(isFavorite(mealId) ? Icons.star : Icons.star_border),
            onPressed: () => {
              toggleFavorite(mealId),
              if (!isDetailScreen) Navigator.of(context).pop(mealId),
            },
          ),
        ],
      ),
    );
  }
}
