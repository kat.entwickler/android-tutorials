import 'package:flutter/cupertino.dart';

class WidgetKey {
  static const adviceFieldKey = Key("adviceFieldKey");
  static const errorFieldKey = Key("errorFieldKey");
  static const initialFieldKey = Key("initialFieldKey");
  static const buttonKey = Key("buttonKey");
}