import 'package:advicer/application/bloc/advicer_new_bloc.dart';
import 'package:advicer/core/constants.dart';
import 'package:advicer/presentation/advicer/widgets/advice_page_body.dart';
import 'package:bloc_test/bloc_test.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockAdviceBloc extends MockBloc<AdvicerEvent, AdvicerState>
    implements AdvicerNewBloc {}

class FakeAdviceEvent extends Fake implements AdvicerEvent {}

class FakeAdviceState extends Fake implements AdvicerState {}

class TestScaffoldWrapper extends StatelessWidget {
  final Widget child;
  const TestScaffoldWrapper({Key? key, required this.child}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: child,
      ),
    );
  }
}

void main() {
  late MockAdviceBloc mockAdviceBloc;

  setUp(() {
    mockAdviceBloc = MockAdviceBloc();
  });

  group("Advice Page Body Widget Test", () {
    setUpAll(() {
      registerFallbackValue(FakeAdviceEvent());
      registerFallbackValue(FakeAdviceState());
    });

    // Define finders that you use over all tests in this group
    final findInitialText = find.byKey(WidgetKey.initialFieldKey);
    final findButton = find.byKey(WidgetKey.buttonKey);
    final findErrorMessage = find.byKey(WidgetKey.errorFieldKey);
    final findProgressIndicator = find.byType(CircularProgressIndicator);

    testWidgets(
        'Advice Page Body should display initial message and button if advice bloc state is initial!',
        (WidgetTester tester) async {
      // Arrange -> define finders -> arrange bloc
      when(() => mockAdviceBloc.state).thenReturn(AdvicerInitial());

      // Build/Pump -> build widget to test
      await tester.pumpWidget(TestScaffoldWrapper(
        child: BlocProvider<AdvicerNewBloc>(
          create: (context) => mockAdviceBloc,
          child: const AdvicePageBody(),
        ),
      ));

      // Check Widget
      expect(findButton, findsOneWidget);
      expect(findInitialText, findsOneWidget);
      expect(findErrorMessage, findsNothing);
    });
  });
}
