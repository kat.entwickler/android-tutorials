import 'package:advicer/infrastructure/datasources/advicer_remote_datasource.dart';
import 'package:advicer/infrastructure/models/advice_model.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_test/flutter_test.dart';
import 'package:http/testing.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import '../../fixtures/fixture_reader.dart';

@GenerateMocks([http.Client])
void main() {
  late AdvicerRemoteDatasource advicerRemoteDatasource;
  late MockClient mockClient;

  /*setUp(() {
    mockClient = MockClient();
    advicerRemoteDatasource = AdvicerRemoteDatasourceImpl(client: mockClient);
  });

  void setUpMockClientSuccess200() {
    when(mockClient.get(any, headers: anyNamed("headers")))
        .thenAnswer((_) async => http.Response(fixture("advice_http_respond.json"), 200)
    );
  }

  void setUpMockClientSuccess404() {
    when(mockClient.get(any, headers: anyNamed("headers")))
        .thenAnswer((_) async => http.Response(fixture("something went wrong"), 404)
    );
  }

  group("getRandomAdviceFromApi", () {
    final t_AdviceModel = AdviceModel(advice: "test", id: 1);

    test("json", () {
      // Arrange
      setUpMockClientSuccess200();
      // Act
      advicerRemoteDatasource.getRandomAdviceFromApi();
      // Assert
      verify(mockClient.get(Uri.parse("https://api.adviceslip.com/advice"),
          headers: {'Content-Type': 'application/json'}
          )
      );
    });
  });*/
}
