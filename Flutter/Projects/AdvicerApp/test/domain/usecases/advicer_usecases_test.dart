
import 'package:advicer/domain/entities/advice_enitity.dart';
import 'package:advicer/domain/failures/failures.dart';
import 'package:advicer/domain/reposetories/advicer_repository.dart';
import 'package:advicer/domain/usecases/advicer_usecases.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'advicer_usecases_test.mocks.dart';

// List mit Klassen die man Faken möchte (Abhängigkeit definieren)
@GenerateMocks([AdvicerRepository])
void main() {

  // Was wir testen wollen
  late AdvicerUsecases advicerUseCases;
  late MockAdvicerRepository mockAdvicerRepository;

  // Zuweisung von di
  setUp((){
    mockAdvicerRepository = MockAdvicerRepository();
    advicerUseCases = AdvicerUsecases(advicerRepository: mockAdvicerRepository);
  });

  group("getAdviceUsecases", () {
    final t_Advicer = AdviceEntity(advice: "Test", id: 1);

    // Passed
    test("Should return the same advice as repo", () async {
      // Arrange
      when(mockAdvicerRepository.getAdviceFromApi()).thenAnswer((_) async => Right(t_Advicer));

      // Act
      final result = await advicerUseCases.getAdviceUsecase();

      // Assert
      // Überprüfung, ob das Ergebnis korrekt ist
      expect(result, Right(t_Advicer));
      // Überprüfung, ob die Funktion aufgerufen wurde
      verify(mockAdvicerRepository.getAdviceFromApi());
      // Keine Interaktion mehr mit der Funktion
      verifyNoMoreInteractions(mockAdvicerRepository);
    });

    // Failed
    test("Should return the same failure as repo", () async {
      // Arrange
      // Hier verwenden wir "equatable" in ServerFailure, damit wir das Ergebnis später vergleichen können.
      when(mockAdvicerRepository.getAdviceFromApi()).thenAnswer((_) async => Left(ServerFailure()));

      // Act
      final result = await advicerUseCases.getAdviceUsecase();

      // Assert
      // Überprüfung, ob das Ergebnis korrekt ist
      expect(result, Left(ServerFailure()));
      // Überprüfung, ob die Funktion aufgerufen wurde
      verify(mockAdvicerRepository.getAdviceFromApi());
      // Keine Interaktion mehr mit der Funktion
      verifyNoMoreInteractions(mockAdvicerRepository);
    });
  });
}
