
import 'package:advicer/application/advicer/advicer_bloc.dart';
import 'package:advicer/domain/entities/advice_enitity.dart';
import 'package:advicer/domain/failures/failures.dart';
import 'package:advicer/domain/usecases/advicer_usecases.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';

import 'advice_bloc_test.mocks.dart';

@GenerateMocks([AdvicerUsecases])
void main() {
  late AdvicerBloc advicerBloc;
  late MockAdvicerUsecases mockAdvicerUsecases;

  setUp(() {
    mockAdvicerUsecases = MockAdvicerUsecases();
    advicerBloc = AdvicerBloc(useCases: mockAdvicerUsecases);
  });

  test("initState should be AdvicerInitial", () {
    // Assert
    expect(advicerBloc.state, equals(AdvicerInitial()));
  });

  group("AdviceRequstedEvent", () {
    final tAdvice = AdviceEntity(advice: "test", id: 1);
    const tAdviceString = "test";

    test("should call usecase if event is added", () async {
      // Arrange
      when(mockAdvicerUsecases.getAdviceUsecase()).thenAnswer((_) async => Right(tAdvice));
      // Act
      advicerBloc.add(AdviceRequestedEvent());
      await untilCalled(mockAdvicerUsecases.getAdviceUsecase());
      // Assert
      verify(mockAdvicerUsecases.getAdviceUsecase());
      verifyNoMoreInteractions(mockAdvicerUsecases);
    });

    test("should emit loading then the loaded state after event is added", () async {
      // Arrange
      when(mockAdvicerUsecases.getAdviceUsecase()).thenAnswer((_) async => Right(tAdvice));
      // Assert late
      final expected = [
        AdvicerStateLoading(),
        AdvicerStateLoaded(advice: tAdviceString)
      ];
      expectLater(advicerBloc.stream, emitsInOrder(expected));

      // Act
      advicerBloc.add(AdviceRequestedEvent());
    });

    test("should emit loading then the loaded state after event is added -> usecase fails -> server failure", () async {
      // Arrange
      when(mockAdvicerUsecases.getAdviceUsecase()).thenAnswer((_) async => Left(ServerFailure()));
      // Assert late
      final expected = [
        AdvicerStateLoading(),
        AdvicerStateError(message: SERVER_FAILURE_MESSAGE)
      ];
      expectLater(advicerBloc.stream, emitsInOrder(expected));

      // Act
      advicerBloc.add(AdviceRequestedEvent());
    });

    test("should emit loading then the loaded state after event is added -> usecase fails -> general failure", () async {
      // Arrange
      when(mockAdvicerUsecases.getAdviceUsecase()).thenAnswer((_) async => Left(GeneralFailure()));
      // Assert late
      final expected = [
        AdvicerStateLoading(),
        AdvicerStateError(message: GENERAL_FAILURE_MESSAGE)
      ];
      expectLater(advicerBloc.stream, emitsInOrder(expected));

      // Act
      advicerBloc.add(AdviceRequestedEvent());
    });
  });
}
