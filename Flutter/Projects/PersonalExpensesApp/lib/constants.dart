import 'model/transaction.dart';

final List<Transaction> _userTransactions = [
  Transaction(
    id: "1",
    title: "Neue Schuhe",
    amount: 69.99,
    date: DateTime.now(),
  ),
  Transaction(
    id: "2",
    title: "Grand Latte",
    amount: 99.99,
    date: DateTime.now(),
  ),
  Transaction(
    id: "3",
    title: "Mütze",
    amount: 9.99,
    date: DateTime.now(),
  ),
  Transaction(
    id: "4",
    title: "Hose",
    amount: 29.99,
    date: DateTime.now(),
  ),
];
