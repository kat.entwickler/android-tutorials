import 'package:flutter/material.dart';


class Answer extends StatelessWidget {
  final Function selectHandler;
  final String answerText;

  const Answer({
    Key? key,
    required this.selectHandler,
    required this.answerText,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      width: double.infinity,
      child: MaterialButton(
        color: Colors.blue,
        textColor: Colors.white,
        child: Text(answerText),
        onPressed: () => selectHandler(),
      ),
    );
  }
}
