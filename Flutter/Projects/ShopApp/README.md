# Shop App

### Aufgabe:
Schreiben Sie eine App namens ShopApp.

Die App soll aus mehreren Screens bestehen. Es soll den Screen Auth, Cart, Edit, Orders, ProductDetail, ProductsOverview, Splash und UserProducts geben.
Verwenden Sie hier das State Managment mit Providern und als Datenbank die Firebase, dazu richten Sie sich eine bitte ein. 


#### Erfordeliches Wissen
 - Wie man Firebase einrichtet


##### Ziele
 - Verschiedene Widges (Scaffold, TextButton, ListView, etc.)
 - Navigation zwischen den Screens
 - Die Möglichkeiten von anklickbaren Objekten (onTap, onPressed, etc.)
 - Models anlegen
 - State Managment
 - Umgang mit Maps
 - Eine DummyData Datei einlegen mit Werten
 - Fonts einbinden und Image
 - Menu erstellen (AppBar, TabBar, BottomNavigationBar)
 - Entfernen von Items aus der Liste durch Sliver
 - Filtern nach bestimmten Kriterien
 - Items ins Favorite einfügen
 - Einsatz von ChangeNotifierProvider/ChangeNotifierProxyProvider/Providers
 - Gezielte Anwendung von Consumern
 - Forms
 - Bei Fehlermeldungen den AlertDialog oder Snakbar Einsatz
 - Firebase verwenden durch die REST-Schnittstelle (Token, userID, etc.)
 - Future für Asynchronität
 - Animationen
