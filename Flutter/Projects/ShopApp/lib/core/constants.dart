class Routes {
  static const String home = '/';
  static const String productDetail = '/product-detail';
  static const String cart = '/cart';
  static const String order = '/order';
  static const String userProducts = '/user-products';
  static const String editProduct = '/edit-product';
  static const String authentication = '/authentication';
}

class Firebase {
  static const urlProducts = 'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/products.json';
  static const urlUserFavorite = 'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/userFavorites/';
  static const urlOrders = 'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/orders.json';
  static const urlUserOrders = 'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/orders/';
  static const signUp = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyDfWfoC2hj_Wg3ZGhS_dJoAs64_XCRf_LU';
  static const signIn = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyDfWfoC2hj_Wg3ZGhS_dJoAs64_XCRf_LU';
}
