import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/model/cart.dart';
import 'package:shop_app/providers/carts.dart';
import 'package:shop_app/providers/orders.dart';
import 'package:shop_app/widget/cart_item.dart';

class CartScreen extends StatelessWidget {
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final carts = Provider.of<Carts>(context);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Your Cart"),
      ),
      body: Column(
        children: [
          Card(
            margin: const EdgeInsets.all(15),
            child: Padding(
              padding: const EdgeInsets.all(15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  const Text(
                    "Total",
                    style: TextStyle(fontSize: 20),
                  ),
                  const Spacer(),
                  Chip(
                    label: Text(
                      "\$${carts.totalAmount.toStringAsFixed(2)}",
                      style: TextStyle(
                        color:
                            Theme.of(context).primaryTextTheme.subtitle1?.color,
                      ),
                    ),
                    backgroundColor: Theme.of(context).primaryColor,
                  ),
                  TextButton(
                    child: const Text("ORDER NOW"),
                    onPressed: carts.totalAmount <= 0 ? null : () async {
                      await Provider.of<Orders>(context, listen: false).addOrder(
                        carts.items.values.toList(),
                        carts.totalAmount,
                      );

                      carts.clear();
                    },
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(
            height: 10,
          ),
          Expanded(
            child: ListView.builder(
              itemCount: carts.items.length,
              itemBuilder: (context, index) {
                return CartItem(
                  cart: Cart(
                    id: carts.items.values.toList()[index].id,
                    title: carts.items.values.toList()[index].title,
                    price: carts.items.values.toList()[index].price,
                    quantity: carts.items.values.toList()[index].quantity,
                  ),
                  productId: carts.items.keys.toList()[index],
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
