import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/model/cart.dart';
import 'package:shop_app/providers/carts.dart';

class CartItem extends StatelessWidget {
  final Cart cart;
  final String productId;

  const CartItem({
    Key? key,
    required this.cart,
    required this.productId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      key: ValueKey(cart.id),
      background: Container(
        color: Theme.of(context).errorColor,
        child: const Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: const EdgeInsets.only(
          right: 20,
        ),
        margin: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
      ),
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        return showDialog(
          context: context,
          builder: (context) => AlertDialog(
            title: const Text("Are you sure?"),
            content: const Text("Do you want to remove the item from your cart?"),
            actions: [
              TextButton(
                child: const Text("YES"),
                onPressed: () => Navigator.of(context).pop(true),
              ),
              TextButton(
                child: const Text("NO"),
                onPressed: () => Navigator.of(context).pop(false),
              ),
            ],
          ),
        );
      },
      onDismissed: (direction) {
        Provider.of<Carts>(context, listen: false).removeItem(productId);
      },
      child: Card(
        margin: const EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 4,
        ),
        child: Padding(
          padding: const EdgeInsets.all(8),
          child: ListTile(
            leading: CircleAvatar(
              child: Padding(
                padding: const EdgeInsets.all(5.0),
                child: FittedBox(
                  child: Text("\$${cart.price}"),
                ),
              ),
            ),
            title: Text(cart.title),
            subtitle: Text("Total: \$${cart.price * cart.quantity}"),
            trailing: Text("${cart.quantity} x"),
          ),
        ),
      ),
    );
  }
}
