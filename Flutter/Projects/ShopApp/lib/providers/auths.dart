import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/core/constants.dart';
import 'dart:async';
import 'package:shop_app/model/http_exception.dart';

class Auth with ChangeNotifier {
  late String? _token;
  DateTime? _expiryDate;
  late String _userId;
  late Timer? _authTimer;

  bool get isAuth => token != null;

  String get userId => _userId;

  String? get token {
    if(_expiryDate != null && _expiryDate?.isAfter(DateTime.now()) == true && _token != null) {
      return _token;
    }

    return null;
  }

  Future<void> login(String email, String password) async {
    return _authenticate(email, password, Firebase.signIn);
  }

  Future<void> signUp(String email, String password) async {
    return _authenticate(email, password, Firebase.signUp);
  }

  Future<void> logout() async {
    _token = "";
    _expiryDate = null;
    _userId = "";

    if(_authTimer != null) {
      _authTimer?.cancel();
      _authTimer = null;
    }

    notifyListeners();

    final prefs = await SharedPreferences.getInstance();
    prefs.remove('userData');
  }

  void _autoLogout() {
    final timeToExpiry = _expiryDate?.difference(DateTime.now()).inSeconds;
    if(timeToExpiry != null) {
      _authTimer = Timer(Duration(seconds: timeToExpiry), logout);
    }
  }

  Future<void> _authenticate(String email, String password, String url) async {
    try {
      final response = await http.post(
        Uri.parse(url),
        body: json.encode(
          {
            'email': email,
            'password': password,
            'returnSecureToken': true,
          },
        ),
      );

      final responseData = json.decode(response.body);
      if(responseData['error'] != null) {
        throw HttpException(message: responseData['error']['message']);
      }

      _token = responseData['idToken'];
      _userId = responseData['localId'];
      _expiryDate = DateTime.now().add(Duration(seconds: int.parse(responseData['expiresIn'])));

      _autoLogout();
      notifyListeners();

      final prefs = await SharedPreferences.getInstance();
      final userData = json.encode({
        'token': _token,
        'userId': _userId,
        'expiryDate': _expiryDate?.toIso8601String(),
      });
      prefs.setString('userData', userData);

    } catch(error) {
      rethrow;
    }
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await SharedPreferences.getInstance();
    if(!prefs.containsKey('userData')) {
      return false;
    }

    final extractedUserData = json.decode(prefs.getString('userData') ?? "") as Map<String, dynamic>;
    final date = extractedUserData['expiryDate'];
    if(date != null) {
      final expiryDate = DateTime.parse(date);
      if(expiryDate.isBefore(DateTime.now())) {
        return false;
      }
    }

    _token = extractedUserData['token'];
    _userId = extractedUserData['userId'];
    _expiryDate = extractedUserData['expiryDate'];
    notifyListeners();
    _autoLogout();
    return true;
  }
}
