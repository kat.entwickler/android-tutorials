import 'package:flutter/material.dart';
import 'package:shop_app/core/constants.dart';
import 'package:shop_app/model/http_exception.dart';
import 'package:shop_app/model/product.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class Products with ChangeNotifier {
  final String authToken;
  final String userId;

  Products(
    this._items, {
    required this.authToken,
    required this.userId,
  });

  List<Product> _items = [
    /* Product(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl:
          'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    Product(
      id: 'p4',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),*/
  ];

  // Nur eine Kopie soll übergeben, damit das Original nicht angefasst wird
  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((productItem) => productItem.isFavorite).toList();
  }

  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    try {
      final filterString = filterByUser ? '&orderBy="creatorId"&equalTo="$userId"' : '';
      final response =
          await http.get(Uri.parse(Firebase.urlProducts + '?auth=$authToken$filterString'));
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProducts = [];

      final favoriteResponse = await http.get(
          Uri.parse(Firebase.urlUserFavorite + "$userId.json?auth=$authToken"));
      final favoriteData = json.decode(favoriteResponse.body);

      extractedData.forEach((productId, productData) {
        loadedProducts.add(
          Product(
            id: productId,
            title: productData['title'],
            description: productData['description'],
            price: productData['price'],
            imageUrl: productData['imageUrl'],
            isFavorite: favoriteData == null ? false : favoriteData[productId] ?? false,
          ),
        );
      });

      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> addProduct(Product product) async {
    try {
      // Erst ausführen, wenn es im Backend (Firebase) abgespeichert wurde
      final response = await http.post(
        Uri.parse(Firebase.urlProducts + "?auth=$authToken"),
        body: jsonEncode(
          {
            'title': product.title,
            'description': product.description,
            'price': product.price,
            'imageUrl': product.imageUrl,
            'creatorId': userId,
          },
        ),
      );

      final newProduct = Product(
        id: jsonDecode(response.body)['name'],
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
      );

      _items.add(newProduct);
      // Teilt allen Listener die neuen Werte an.
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> updateProduct(String id, Product newProduct) async {
    final productIndex = _items.indexWhere((product) => product.id == id);

    if (productIndex >= 0) {
      try {
        final url =
            'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/products/$id.json?auth=$authToken';
        await http.patch(Uri.parse(url),
            body: json.encode({
              'title': newProduct.title,
              'description': newProduct.description,
              'price': newProduct.price,
              'imageUrl': newProduct.imageUrl,
            }));

        // Ersetzten das alte Produkt mit neuen Werten
        _items[productIndex] = newProduct;
      } catch (error) {
        rethrow;
      }
    } else {
      print("Error, updating product!");
    }

    notifyListeners();
  }

  Future<void> deleteProduct(String id) async {
    try {
      final url =
          'https://flutter-shop-a6d22-default-rtdb.firebaseio.com/product/$id.json?auth=$authToken';
      final existingProductIndex =
          _items.indexWhere((product) => product.id == id);
      var existingProduct = _items[existingProductIndex];
      _items.removeAt(existingProductIndex);
      notifyListeners();

      final response = await http.delete(Uri.parse(url));
      if (response.statusCode >= 400) {
        _items.insert(existingProductIndex, existingProduct);
        notifyListeners();
        throw HttpException(message: "Could not delete product!");
      }

      existingProduct = Product(
        id: "",
        title: "",
        description: "",
        price: 0.0,
        imageUrl: "",
      );
    } catch (error) {
      rethrow;
    }
  }

  Product findById(String id) {
    return _items.firstWhere((product) => product.id == id);
  }
}
