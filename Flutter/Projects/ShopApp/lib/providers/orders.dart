import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shop_app/core/constants.dart';
import 'package:shop_app/model/cart.dart';
import 'package:shop_app/model/order.dart';
import 'package:http/http.dart' as http;

class Orders with ChangeNotifier {
  List<Order> _orders = [];
  final String authToken;
  final String userId;

  Orders(
    this._orders, {
    required this.authToken,
    required this.userId,
  });

  List<Order> get orders => [..._orders];

  Future<void> fetchAndSetOrders() async {
    try {
      // Erst ausführen, wenn es im Backend (Firebase) abgespeichert wurde
      final response =
          await http.get(Uri.parse(Firebase.urlUserOrders + "$userId.json?auth=$authToken"));
      final List<Order> loadedOrders = [];
      // TODO Fehlerbehebung fällt -> z.B. keine Orders vorhanden oder sonst was
      final extractedData = json.decode(response.body) as Map<String, dynamic>;

      extractedData.forEach((orderId, orderData) {
        loadedOrders.add(
          Order(
            id: orderId,
            amount: orderData['amount'],
            products: (orderData['products'] as List<dynamic>)
                .map(
                  (item) => Cart(
                    id: item['id'],
                    title: item['title'],
                    price: item['price'],
                    quantity: item['quantity'],
                  ),
                )
                .toList(),
            dateTime: DateTime.parse(orderData['dateTime']),
          ),
        );
      });

      _orders = loadedOrders;
      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }

  Future<void> addOrder(List<Cart> cartProducts, double total) async {
    final timestamp = DateTime.now();

    try {
      // Erst ausführen, wenn es im Backend (Firebase) abgespeichert wurde
      final response = await http.post(
        Uri.parse(Firebase.urlUserOrders + "$userId.json?auth=$authToken"),
        body: jsonEncode(
          {
            'amount': total,
            'products': cartProducts
                .map(
                  (cartProduct) => {
                    'id': cartProduct.id,
                    'title': cartProduct.title,
                    'quantity': cartProduct.quantity,
                    'price': cartProduct.price,
                  },
                )
                .toList(),
            'dateTime': timestamp.toIso8601String(),
          },
        ),
      );

      _orders.insert(
          0,
          Order(
            id: json.decode(response.body)['name'],
            amount: total,
            products: cartProducts,
            dateTime: timestamp,
          ));

      notifyListeners();
    } catch (error) {
      rethrow;
    }
  }
}
