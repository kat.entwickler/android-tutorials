import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/core/constants.dart';
import 'package:shop_app/providers/auths.dart';
import 'package:shop_app/providers/carts.dart';
import 'package:shop_app/providers/orders.dart';
import 'package:shop_app/providers/products.dart';
import 'package:shop_app/screens/auth_screen.dart';
import 'package:shop_app/screens/cart_screen.dart';
import 'package:shop_app/screens/edit_product_screen.dart';
import 'package:shop_app/screens/orders_screen.dart';
import 'package:shop_app/screens/product_detail_screen.dart';
import 'package:shop_app/screens/products_overview_screen.dart';
import 'package:shop_app/screens/splash_screen.dart';
import 'package:shop_app/screens/user_products_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: Auth()),
        // ProxyProvider wartet bis der ChangeNotifierProvider mit Auth() fertig wird und erzeugt erst dann sich
        // Auch bei Änderungen wird dieser angepasst
        ChangeNotifierProxyProvider<Auth, Products>(
          create: (context) => Products([], authToken: "", userId: ""),
          update: (_, auth, previousProducts) => Products(
            previousProducts == null ? [] : previousProducts.items,
            authToken: auth.token ?? "",
            userId: auth.userId,
          ),
        ),
        ChangeNotifierProvider.value(value: Carts()),
        ChangeNotifierProxyProvider<Auth, Orders>(
          create: (context) => Orders([], authToken: "", userId: ""),
          update: (_, auth, previousProducts) => Orders(
            previousProducts == null ? [] : previousProducts.orders,
            authToken: auth.token ?? "",
            userId: auth.userId,
          ),
        ),
      ],
      child: Consumer<Auth>(builder: (context, auth, _) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          title: "Shop App",
          theme: ThemeData(
            fontFamily: "Lato",
            primarySwatch: Colors.purple,
            colorScheme: ThemeData.light().colorScheme.copyWith(
                  secondary: Colors.deepOrange,
                ),
          ),
          home:
              //auth.isAuth ? const AuthScreen() : const ProductsOverviewScreen(),
              auth.isAuth
                  ? const ProductsOverviewScreen()
                  : FutureBuilder(
                      future: auth.tryAutoLogin(),
                      builder: (context, authResultSnapshot) =>
                          authResultSnapshot.connectionState ==
                                  ConnectionState.waiting
                              ? SplashScreen()
                              : const AuthScreen(),
                    ),
          routes: {
            Routes.productDetail: (context) => const ProductDetailScreen(),
            Routes.cart: (context) => const CartScreen(),
            Routes.order: (context) => const OrdersScreen(),
            Routes.userProducts: (context) => const UserProductsScreen(),
            Routes.editProduct: (context) => const EditProductScreen(),
          },
        );
      }),
    );
  }
}
