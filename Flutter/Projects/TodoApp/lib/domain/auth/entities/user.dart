import 'package:todo_app/domain/auth/entities/id.dart';

class CustomUser {
  final UniqueID id;

  CustomUser({required this.id});
}
