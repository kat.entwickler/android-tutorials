import 'package:uuid/uuid.dart';

class UniqueID {
  const UniqueID._(this.value);

  final String value;

  // Selber eine einzigartige ID generieren aus der App für ein neues Todo
  factory UniqueID() {
    return UniqueID._(const Uuid().v1());
  }

  // Eine einzigartige ID von außen importieren (hier Firebase)
  factory UniqueID.fromUniqueString(String uniqueID) {
    return UniqueID._(uniqueID);
  }
}
