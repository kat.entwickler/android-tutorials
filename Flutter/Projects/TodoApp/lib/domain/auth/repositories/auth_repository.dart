import 'package:dartz/dartz.dart';
import 'package:todo_app/core/failures/auth_failures.dart';
import 'package:todo_app/domain/auth/entities/user.dart';

abstract class AuthRepository {
  Future<Either<AuthFailures, Unit>> signInWithEmailAndPassword({required String email, required String password});

  Future<Either<AuthFailures, Unit>> registerWithEmailAndPassword({required String email, required String password});

  Future<void> signOut();

  Option<CustomUser> getSignedInUser();
}
