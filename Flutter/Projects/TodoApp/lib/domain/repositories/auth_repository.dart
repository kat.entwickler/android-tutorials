import 'package:dartz/dartz.dart';
import 'package:todo_app/core/failures/auth_failures.dart';

abstract class AuthRepository {
  Future<Either<AuthFailures, Unit>> signInWithEmailAndPassword({required String email, required String password});

  Future<Either<AuthFailures, Unit>> registerWithEmailAndPassword({required String email, required String password});
}
