import 'package:todo_app/domain/auth/entities/id.dart';
import 'package:todo_app/domain/cloud/entities/todo_color.dart';

class Todo {
  final UniqueID id;
  final String title;
  final String body;
  final bool done;
  final TodoColor color;

  Todo({
      required this.id,
      required this.title,
      required this.body,
      required this.done,
      required this.color
  });

  factory Todo.empty() {
    return Todo(
        id: UniqueID(),
        title: "",
        body: "",
        done: false,
        color: TodoColor(color: TodoColor.predefinedColors[5]));
  }

  // CopyWith nimmt den aktuellen Wert und überschreibt diesen dann.
  Todo copyWith({
    UniqueID? id, // "?" sagt aus, dass es null sein kann
    String? title,
    String? body,
    bool? done,
    TodoColor? color,
  }) {
    return Todo(
      // Wird entschieden, ob eine neue id genommen wird oder die alte, je nach dem ob id null ist
      id: id ?? this.id,
      title: title ?? this.title,
      body: body ?? this.body,
      done: done ?? this.done,
      color: color ?? this.color,
    );
  }
}
