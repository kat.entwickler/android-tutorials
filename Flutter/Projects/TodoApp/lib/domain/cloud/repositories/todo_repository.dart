import 'package:dartz/dartz.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';


abstract class TodoRepository {
  // Stream, weil wir auf die Änderungen reagieren können
  Stream<Either<TodoFailure, List<Todo>>> watchAll();

  Future<Either<TodoFailure, Unit>> create(Todo todo);

  Future<Either<TodoFailure, Unit>> update(Todo todo);

  // Kann auch über id gemacht werden
  Future<Either<TodoFailure, Unit>> delete(Todo todo);
}
