import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:dartz/dartz.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';
import 'package:todo_app/domain/cloud/repositories/todo_repository.dart';
import 'package:todo_app/infrastructure/extensions/firebase_helpers.dart';
import 'package:todo_app/infrastructure/models/todo_model.dart';


class TodoRepositoryImpl implements TodoRepository {
  final FirebaseFirestore firebaseFirestore;

  TodoRepositoryImpl({required this.firebaseFirestore});

  @override
  Stream<Either<TodoFailure, List<Todo>>> watchAll() async*{
    final userDoc = await firebaseFirestore.userDocument();
    // Wir packen etwas ins Stream
    yield* userDoc.todoCollection
        .snapshots()
        .map((snapshot) => right<TodoFailure, List<Todo>>(snapshot.docs
        .map((doc) => TodoModel.fromFirestore(doc).toDomain())
        .toList()))

    // error handling (left side)
        .handleError((e) {
      if (e is FirebaseException) {
        if (e.code.contains('permission-denied') || e.code.contains("PERMISSION_DENIED")) {
          return left(InsufficientPermissions());
        } else {
          return left(UnexpectedFailure());
        }
      } else {
        return left(UnexpectedFailure());
      }
    });
  }

  @override
  Future<Either<TodoFailure, Unit>> create(Todo todo) async{
    try {
      final userDoc = await firebaseFirestore.userDocument();
      final todoModel = TodoModel.fromDomain(todo);

      await userDoc.todoCollection.doc(todoModel.id).set(todoModel.toMap());
      return right(unit);
    } on FirebaseException catch(e) {
      if (e.code.contains("PERMISSION_DENIED")) {
        return left(InsufficientPermissions());
      } else {
        return left(UnexpectedFailure());
      }
    }
  }

  @override
  Future<Either<TodoFailure, Unit>> delete(Todo todo) async{
    try {
      final userDoc = await firebaseFirestore.userDocument();
      final todoModel = TodoModel.fromDomain(todo);

      await userDoc.todoCollection.doc(todoModel.id).delete();
      return right(unit);
    } on FirebaseException catch(e) {
      if (e.code.contains("PERMISSION_DENIED")) {
        return left(InsufficientPermissions());
      }else if (e.code.contains("NOT_FOUND")) {
        return left(NotFound());
      } else {
        return left(UnexpectedFailure());
      }
    }
  }

  @override
  Future<Either<TodoFailure, Unit>> update(Todo todo) async{
    try {
      final userDoc = await firebaseFirestore.userDocument();
      final todoModel = TodoModel.fromDomain(todo);

      await userDoc.todoCollection.doc(todoModel.id).update(todoModel.toMap());
      return right(unit);
    } on FirebaseException catch(e) {
      if (e.code.contains("PERMISSION_DENIED")) {
        return left(InsufficientPermissions());
      }else if (e.code.contains("NOT_FOUND")) {
        return left(NotFound());
      } else {
        return left(UnexpectedFailure());
      }
    }
  }
}
