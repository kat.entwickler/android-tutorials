import 'package:dartz/dartz.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_app/core/failures/auth_failures.dart';
import 'package:todo_app/domain/auth/entities/user.dart';
import 'package:todo_app/domain/auth/repositories/auth_repository.dart';
import 'package:todo_app/infrastructure/extensions/firebase_user_mapper.dart';


class AuthRepositoryImp implements AuthRepository {
  final FirebaseAuth firebaseAuth;

  AuthRepositoryImp({required this.firebaseAuth});

  @override
  Future<Either<AuthFailures, Unit>> signInWithEmailAndPassword({required String email, required String password}) async {
    try {
      await firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      return right(unit);
    } on FirebaseAuthException catch(e) {
      if (e.code == "wrong-password" || e.code == "user-not-found") {
        return left(InvalidEmailAndPasswordCombinationFailure());
      } else {
        return left(ServerFailure());
      }
    }
  }

  @override
  Future<Either<AuthFailures, Unit>> registerWithEmailAndPassword({required String email, required String password}) async {
    try {
      await firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      return right(unit);
    } on FirebaseAuthException catch(e) {
      if (e.code == "email-already-in-use") {
        return left(EmailAlreadyInUseFailure());
      } else {
        return left(ServerFailure());
      }
    }
  }

  @override
  Future<void> signOut() => Future.wait([
    firebaseAuth.signOut()
  ]);

  @override
  Option<CustomUser> getSignedInUser() => optionOf(firebaseAuth.currentUser?.toDomain());
}
