import 'package:firebase_auth/firebase_auth.dart';
import 'package:todo_app/domain/auth/entities/id.dart';
import 'package:todo_app/domain/auth/entities/user.dart';


// Das Objekt aus Firebase (User) wollen wir auf unseren CustomUser anpassen
extension FirebaseUserMapper on User{
  // Convertiert
  CustomUser toDomain() {
    // uid kommt von Firebase, muss genau so heißen
    return CustomUser(id: UniqueID.fromUniqueString(uid));
  }
}
