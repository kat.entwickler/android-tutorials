import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/core/failures/auth_failures.dart';
import 'package:todo_app/domain/auth/repositories/auth_repository.dart';

part 'sign_up_form_event.dart';

part 'sign_up_form_state.dart';


class SignUpFormBloc extends Bloc<SignUpFormEvent, SignUpFormState> {
  final AuthRepository authRepository;

  SignUpFormBloc({required this.authRepository}) : super(SignUpFormState(isSubmitting: false, showValidationMessages: false, authFailureOrSuccessOption: none())) {
    on<SignInWithEmailAndPasswordPressed>((event, emit) async {
      if (event.email == null || event.password == null) {
        emit(state.copyWith(isSubmitting: false, showValidationMessages: true));
      } else {
        emit(state.copyWith(isSubmitting: true, showValidationMessages: false));
        //emit(SignUpFormState(isSubmitting: true, showValidationMessages: false));
        // TODO Wie löst man das ohne "!"
        final failureOrSuccess = await authRepository.signInWithEmailAndPassword(email: event.email!, password: event.password!);
        emit(state.copyWith(isSubmitting: false, authFailureOrSuccessOption: optionOf(failureOrSuccess)));
      }
    });

    on<RegisterWithEmailAndPasswordPressed>((event, emit) async {
      if (event.email == null || event.password == null) {
        emit(state.copyWith(isSubmitting: false, showValidationMessages: true));
      } else {
        emit(state.copyWith(isSubmitting: true, showValidationMessages: false));
        //emit(SignUpFormState(isSubmitting: true, showValidationMessages: false));
        // TODO Wie löst man das ohne "!"
        final failureOrSuccess = await authRepository.registerWithEmailAndPassword(email: event.email!, password: event.password!);
        print(failureOrSuccess.runtimeType);
        print(failureOrSuccess);
        emit(state.copyWith(isSubmitting: false, authFailureOrSuccessOption: optionOf(failureOrSuccess)));
      }
    });
  }
}
