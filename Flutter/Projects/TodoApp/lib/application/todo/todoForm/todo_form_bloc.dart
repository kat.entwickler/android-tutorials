import 'dart:ui';

import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';
import 'package:todo_app/domain/cloud/entities/todo_color.dart';
import 'package:todo_app/domain/cloud/repositories/todo_repository.dart';

part 'todo_form_event.dart';
part 'todo_form_state.dart';


class TodoFormBloc extends Bloc<TodoFormEvent, TodoFormState> {
  final TodoRepository todoRepository;
  TodoFormBloc({required this.todoRepository}) : super(TodoFormState.initial()) {

    on<InitializeTodoDetailPage>((event, emit) {
      if (event.todo != null) {
        // Ein Toddo wird editiert
        emit(state.copyWith(todo: event.todo, isEditing: true));
      } else {
        // Ein leeres Toddo zum ausfüllen
        emit(state);
      }
    });

    // Reagieren auf das Event
    on<ColorChangedEvent>((event, emit) {
      /* Es soll nur den State aktuallisieren
       * Schritt 1: Den aktuellen State ändern (Enthält unser Toddo)
       * Schritt 2: Den aktuellen Toddo nehmen und die Farbet aus dem Event nehmen und vergleichen
       */
      emit(state.copyWith(todo: state.todo.copyWith(color: TodoColor(color: event.color))));
    });

    on<SavePressedEvent>((event, emit) async{
      Either<TodoFailure, Unit>? failureOrSuccess;

      // Damit wir Loadingindicator anzeigen können
      emit(state.copyWith(isSaving: true, failureOrSuccessOption: none()));

      // Prüfen ob wir etwas in Event ist
      if (event.title != null && event.body != null) {
        // Nehmen das aktuelle Toddo aus dem Event
        final Todo editedTodo = state.todo.copyWith(title: event.title, body: event.body);

        // Festellen ob wir editieren oder ein neue Toddo erstellen
        if (state.isEditing) {
          print("1");
          failureOrSuccess = await todoRepository.update(editedTodo);
        } else {
          print("2");
          failureOrSuccess = await todoRepository.create(editedTodo);
        }

        // Hier wollen wir dem State die Änderungen mit teillein oder einen Fehler
        // optionOF() -> irgendetwas: dann same(), sonst none()
        emit(state.copyWith(isSaving: false, showErrorMessages: true, failureOrSuccessOption: optionOf(failureOrSuccess)));
      }
    });
  }
}
