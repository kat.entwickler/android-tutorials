import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';
import 'package:todo_app/domain/cloud/repositories/todo_repository.dart';

part 'controller_event.dart';
part 'controller_state.dart';


class ControllerBloc extends Bloc<ControllerEvent, ControllerState> {
  final TodoRepository todoRepository;

  ControllerBloc({required this.todoRepository}) : super(ControllerInitial()) {

    on<UpdateTodoEvent>((event, emit) async{
      emit(ControllerInProgress());
      // Das Event liefert unseres Toddo. Mit der copyWith-Funktion können wir bestimmte Attribute ändern
      final failureOrSuccess = await todoRepository.update(event.todo.copyWith(done: event.done));
      failureOrSuccess.fold(
              (failure) => emit(ControllerFailure(todoFailure: failure)),
              (r) => emit(ControllerSuccess())
      );
    });

    on<DeleteTodoEvent>((event, emit) async{
      emit(ControllerInProgress());
      // Das Event liefert unseres Toddo
      final failureOrSuccess = await todoRepository.delete(event.todo);
      failureOrSuccess.fold(
              (failure) => emit(ControllerFailure(todoFailure: failure)),
              (r) => emit(ControllerSuccess())
      );
    });
  }
}
