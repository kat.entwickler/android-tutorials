import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:flutter/material.dart';
import 'package:meta/meta.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';
import 'package:todo_app/domain/cloud/repositories/todo_repository.dart';

part 'observer_event.dart';
part 'observer_state.dart';


class ObserverBloc extends Bloc<ObserverEvent, ObserverState> {
  final TodoRepository todoRepository;
  StreamSubscription<Either<TodoFailure, List<Todo>>>? _todoStreamSub;

  ObserverBloc({required this.todoRepository}) : super(ObserverInitial()) {
    on<ObserveAllEvent>((event, emit) async{
      emit(ObserverLoading());
      // Falls schon ein StreamSubscription vorhanden ist, dann schließe es
      await _todoStreamSub?.cancel();
      // Erstelle ein neuen StreamSubscription
      _todoStreamSub = todoRepository.watchAll().listen((failureOrTodos) => add(
          TodosUpdatedEvent(failureOrTodos: failureOrTodos)
      ));
    });

    // Ein Event aus einem anderen aufrufen
    on<TodosUpdatedEvent>((event, emit) {
      print(event.failureOrTodos);
      event.failureOrTodos.fold(
              (failures) => emit(ObserverFailure(todoFailure: failures)),
              (todos) => emit(ObserverSuccess(todos: todos))
      );
    });
  }

  // Muss gemacht werden, damit der Stream auch beim Schließen der App beendet wird.
  @override
  Future<void> close() async {
    await _todoStreamSub?.cancel();
    return super.close();
  }
}
