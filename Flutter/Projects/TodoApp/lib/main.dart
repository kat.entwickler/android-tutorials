import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/injection.dart' as di;
import 'package:todo_app/presentation/routes/router.gr.dart' as r;
import 'package:todo_app/theme.dart';

import 'application/authbloc/auth_bloc.dart';
import 'injection.dart';


void main() async {
  // Um sicher zu gehen, dass keine Widgets davor gebaut worden sind.
  WidgetsFlutterBinding.ensureInitialized();
  // Firebase für die App einrichten
  await Firebase.initializeApp();
  await di.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final _appRouter = r.AppRouter();

  MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          // Gleich prüfen, ob man autofeziert ist
          create: (context) => sl<AuthBloc>()..add(AuthCheckRequestedEvent()),
        )
      ],
      child: MaterialApp.router(
          // Navigiert nach dem Login zu meiner HomePage
          routeInformationParser: _appRouter.defaultRouteParser(),
          routerDelegate: _appRouter.delegate(),
          debugShowCheckedModeBanner: false,
          title: 'TODO',
          theme: AppTheme.lightTheme,
          darkTheme: AppTheme.darkTheme,
          themeMode: ThemeMode.dark
      ),
    );
  }
}
