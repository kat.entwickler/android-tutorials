import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get_it/get_it.dart';
import 'package:todo_app/application/authbloc/auth_bloc.dart';
import 'package:todo_app/application/signupform/sign_up_form_bloc.dart';
import 'package:todo_app/application/todo/todoForm/todo_form_bloc.dart';
import 'package:todo_app/domain/auth/repositories/auth_repository.dart';
import 'package:todo_app/domain/cloud/repositories/todo_repository.dart';
import 'package:todo_app/infrastructure/repositories/auth_repository_impl.dart';
import 'package:todo_app/infrastructure/repositories/todo_repository_imp.dart';

import 'application/todo/controller/controller_bloc.dart';
import 'application/todo/observer/observer_bloc.dart';


final sl = GetIt.I; // sl == service locator

Future<void> init() async {
  // State management
  sl.registerFactory(() => SignUpFormBloc(authRepository: sl()));
  sl.registerFactory(() => AuthBloc(authRepository: sl()));
  sl.registerFactory(() => ObserverBloc(todoRepository: sl()));
  sl.registerFactory(() => ControllerBloc(todoRepository: sl()));
  sl.registerFactory(() => TodoFormBloc(todoRepository: sl()));

  // Repositories
  sl.registerLazySingleton<AuthRepository>(() => AuthRepositoryImp(firebaseAuth: sl()));
  sl.registerLazySingleton<TodoRepository>(() => TodoRepositoryImpl(firebaseFirestore: sl()));

  // Extern
  final firebaseAuth = FirebaseAuth.instance;
  sl.registerLazySingleton(() => firebaseAuth);

  final firebaseFirestore = FirebaseFirestore.instance;
  sl.registerLazySingleton(() => firebaseFirestore);
}
