abstract class AuthFailures {}

class ServerFailure extends AuthFailures {}

class EmailAlreadyInUseFailure extends AuthFailures {}

class InvalidEmailAndPasswordCombinationFailure extends AuthFailures {}
