import 'package:auto_route/auto_route.dart';
import 'package:todo_app/presentation/home/home_page.dart';
import 'package:todo_app/presentation/login/signup_page.dart';
import 'package:todo_app/presentation/splash/splash_page.dart';
import 'package:todo_app/presentation/todo_detail/todo_detail_page.dart';

// Befehl zum ausführen: flutter packages pub run build_runner build
@MaterialAutoRouter(
    routes: <AutoRoute>[
      AutoRoute(page: SplashPage, initial: true),
      AutoRoute(page: SignUpPage, initial: false),
      AutoRoute(page: HomePage, initial: false),
      AutoRoute(page: TodoDetailPage, initial: false, fullscreenDialog: true)
    ]
)

class $AppRouter {}
