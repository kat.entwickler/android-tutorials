import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/application/authbloc/auth_bloc.dart';
import 'package:todo_app/application/todo/controller/controller_bloc.dart';
import 'package:todo_app/application/todo/observer/observer_bloc.dart';
import 'package:todo_app/core/failures/todo_failures.dart';
import 'package:todo_app/injection.dart';
import 'package:todo_app/presentation/home/widgets/home_body.dart';
import 'package:todo_app/presentation/routes/router.gr.dart';


class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);
  
  // Funktion
  String _mapFailureToMessage(TodoFailure todoFailure) {
    switch (todoFailure.runtimeType) {
      case InsufficientPermissions: return "You have not the permissions to do that";
      case UnexpectedFailure: return "Something went wrong";
      default: return "Something went wrong";
    }
  }

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    final observerBloc = sl<ObserverBloc>()..add(ObserveAllEvent());
    final controllerBloc = sl<ControllerBloc>();

    return MultiBlocProvider(
      providers: [
        BlocProvider<ObserverBloc>(create: (context) => observerBloc),
        BlocProvider<ControllerBloc>(create: (context) => controllerBloc)
      ],
      child: MultiBlocListener(
        listeners: [
          BlocListener<AuthBloc, AuthState>(
            listener: (context, state) {
              if (state is AuthStateUnauthenticated) {
                AutoRouter.of(context).push(const SignUpPageRoute());
              }
            }
          ),
          BlocListener<ControllerBloc, ControllerState>(
            listener: (context, state) {
              if (state is ControllerFailure) {
                ScaffoldMessenger.of(context).showSnackBar(
                  SnackBar(
                    backgroundColor: themeData.colorScheme.onError,
                    content: Text(_mapFailureToMessage(state.todoFailure))
                  )
                );
              }
            }
          )
        ],
        child: Scaffold(
          appBar: AppBar(
            leading: IconButton(
              onPressed: (){
                BlocProvider.of<AuthBloc>(context).add(SignOutPressedEvent());
              },
              icon: const Icon(Icons.exit_to_app),
            ),
              title: const Text("Todo")),
          body: const HomeBody(),
          floatingActionButton: FloatingActionButton(
            backgroundColor: themeData.colorScheme.secondary,
            child: const Icon(Icons.add, size: 26),
            onPressed: () {
              // Navigiere zu TodoDetail, weil null übergeben wird, das bedeutet hier wird neues Toddo erstellt
              AutoRouter.of(context).push(TodoDetailPageRoute(todo: null));
            }
          )
        )
      )
    );
  }
}
