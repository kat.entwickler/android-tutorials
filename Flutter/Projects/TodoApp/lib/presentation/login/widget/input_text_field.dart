import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todo_app/application/signupform/sign_up_form_bloc.dart';

class InputTextField extends StatefulWidget {
  final String labelText;
  final SignUpFormState state;
  final String? validateEmail;

  const InputTextField({
    Key? key,
    required this.labelText,
    required this.state,
    required this.validateEmail
  }) : super(key: key);

  @override
  State<InputTextField> createState() => _InputTextFieldState();
}

class _InputTextFieldState extends State<InputTextField> {

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);
    final String validatorText;

    if (widget.validateEmail != null) {
      validatorText = widget.validateEmail!;
    } else {
      validatorText = "";
    }

    return TextFormField(
      cursorColor: themeData.colorScheme.secondary,
      autovalidateMode: widget.state.showValidationMessages ? AutovalidateMode.always : AutovalidateMode.disabled,
      // Der Validator erhält den Text, den der Benutzer eingegeben hat.
      validator: (value) {
        if (value == null || value.isEmpty) {
          return widget.validateEmail;
        } else {
          return null;
        }
      },
      //obscureText: true, // Passwort mit ****
      decoration: InputDecoration(
          labelText: widget.labelText
      ),
    );
  }
}
