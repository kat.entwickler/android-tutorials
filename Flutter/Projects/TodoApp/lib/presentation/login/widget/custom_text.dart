import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CustomText extends StatelessWidget {
  final String title;
  final double fontSize;
  const CustomText({Key? key, required this.title, required this.fontSize}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);

    return Text(
        title,
        style: themeData.textTheme.headline1?.copyWith(
            fontSize: fontSize,
            fontWeight: FontWeight.w500,
            letterSpacing: 4
        )
    );
  }
}
