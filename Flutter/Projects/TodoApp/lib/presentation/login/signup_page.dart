import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/application/signupform/sign_up_form_bloc.dart';
import 'package:todo_app/injection.dart';
import 'package:todo_app/presentation/login/widget/signup_form.dart';

class SignUpPage extends StatelessWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocProvider(
        // Zuweisung dem service Locate einen bestimmten Bloc
        create: (context) => sl<SignUpFormBloc>(),
        child: const SignUpForm()
      ),
    );
  }
}
