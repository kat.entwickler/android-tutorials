import 'package:auto_route/auto_route.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/application/authbloc/auth_bloc.dart';
import 'package:todo_app/presentation/routes/router.gr.dart';


class SplashPage extends StatelessWidget {
  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is AuthStateAuthenticated) {
          AutoRouter.of(context).push(const HomePageRoute());
        } else if (state is AuthStateUnauthenticated) {
          // Alternative zu AutoRouter.of()
          context.router.replace(const SignUpPageRoute());
        }
      },
      child: Scaffold(
        body: Center(
          child: CircularProgressIndicator(
            color: Theme
                .of(context)
                .colorScheme
                .secondary,
          ),
        ),
      ),
    );
  }
}
