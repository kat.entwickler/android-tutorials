import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/application/todo/todoForm/todo_form_bloc.dart';
import 'package:todo_app/presentation/core/custom_button.dart';
import 'package:todo_app/presentation/todo_detail/widgets/color_field.dart';


class TodoForm extends StatelessWidget {
  const TodoForm({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<TodoFormBloc>(context);
    final GlobalKey<FormState> formKey = GlobalKey<FormState>();
    final textEditingControllerTitle = TextEditingController();
    final textEditingControllerBody = TextEditingController();

    late String body;
    late String title;

    // Functions
    String? validateBody(String? input) {
      if (input == null || input.isEmpty) {
        return "please enter a description";
      } else if (input.length < 300) {
        body = input;
        return null;
      } else {
        return "body too long";
      }
    }

    String? validateTitle(String? input) {
      if (input == null || input.isEmpty) {
        return "please enter a title";
      } else if (input.length < 30) {
        title = input;
        return null;
      } else {
        return "title too long";
      }
    }

    return BlocConsumer<TodoFormBloc, TodoFormState>(
      listenWhen: (p,c) => p.isEditing != c.isEditing,
      // Um gewisse Funktionen auszuführen, wenn state sich ändert
      listener: (context, state) {
        textEditingControllerTitle.text = state.todo.title;
        textEditingControllerBody.text = state.todo.body;
      },
      // UI soll reagieren auf den state
      builder: (context, state) {
        return Padding(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Form(
            key: formKey,
            autovalidateMode: state.showErrorMessages ? AutovalidateMode.always : AutovalidateMode.disabled,
            child: ListView(
              children: [
                TextFormField(
                  controller: textEditingControllerTitle,
                  cursorColor: Colors.white,
                  maxLength: 100,
                  minLines: 1,
                  maxLines: 2,
                  validator: validateTitle,
                  decoration: InputDecoration(
                    labelText: "Title",
                    counterText: "",
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8)
                    )
                  )
                ),
                const SizedBox(height: 20),
                TextFormField(
                  controller: textEditingControllerBody,
                    cursorColor: Colors.white,
                    maxLength: 300,
                    minLines: 5,
                    maxLines: 8,
                    validator: validateBody,
                    decoration: InputDecoration(
                        labelText: "Body",
                        counterText: "",
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(8)
                        )
                    )
                ),
                const SizedBox(height: 20),
                ColorField(color: state.todo.color),
                const SizedBox(height: 20),
                CustomButton(buttonText: "Save", callback: () {
                  if (formKey.currentState!.validate()) {
                    bloc.add(SavePressedEvent(title: title, body: body));
                  } else { // Auf Fehler reagieren
                    bloc.add(SavePressedEvent(title: null, body: null));

                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        backgroundColor: Colors.redAccent,
                        content: Text(
                          "Invalid Input",
                          style: Theme.of(context).textTheme.bodyText1,
                        )));
                  }
                })
              ]
            )
          )
        );
      }
    );
  }
}
