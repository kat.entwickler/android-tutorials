import 'package:flutter/material.dart';


class SaveProgressOverlay extends StatelessWidget {
  final bool isSaving;
  const SaveProgressOverlay({Key? key, required this.isSaving}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      // Verhindert das wir nichts ankicken können.
      ignoring: !isSaving,
      child: Visibility(
        // Dieses Widget wird nur angezeigt, wenn isSaving = true ist
        visible: isSaving,
        child: Center(
          child: CircularProgressIndicator(
            color: Theme.of(context).colorScheme.secondary
          )
        )
      )
    );
  }
}
