import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:todo_app/application/todo/todoForm/todo_form_bloc.dart';
import 'package:todo_app/domain/cloud/entities/todo.dart';
import 'package:todo_app/presentation/routes/router.gr.dart';
import 'package:todo_app/presentation/todo_detail/widgets/save_progress_overlay.dart';
import 'package:todo_app/presentation/todo_detail/widgets/todo_form.dart';
import '../../injection.dart';


class TodoDetailPage extends StatelessWidget {
  final Todo? todo;

  const TodoDetailPage({Key? key, required this.todo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final themeData = Theme.of(context);

    return BlocProvider(
      create: (context) => sl<TodoFormBloc>()..add(InitializeTodoDetailPage(todo: todo)),
      // Initialisiert unser Toddo auf der Page
      child: BlocConsumer<TodoFormBloc, TodoFormState>(
        // Nur auf einen bestimmten Fall hören
        listenWhen: (p,c) => p.failureOrSuccessOption != c.failureOrSuccessOption,
        listener: (context, state) {
          state.failureOrSuccessOption.fold(
            () => null,
            (eitherFailureOrSuccess) => eitherFailureOrSuccess.fold(
                (failure) => ScaffoldMessenger.of(context).showSnackBar(
                    SnackBar(
                        content: const Text("Failure"),
                        backgroundColor: themeData.colorScheme.onError,
                    )
                ),
                (_) => Navigator.of(context).popUntil((route) => route.settings.name == HomePageRoute.name)
            )
          );
        },
        builder: (context, state) {
          return Scaffold(
              appBar: AppBar(
                  centerTitle: true,
                  title: Text(todo == null ? "Create Todo" : "Edit Todo")
              ),
              body: Stack(
                children: [
                  const TodoForm(),
                  SaveProgressOverlay(isSaving: state.isSaving)
                ]
              )
          );
        }
      )
    );
  }
}
