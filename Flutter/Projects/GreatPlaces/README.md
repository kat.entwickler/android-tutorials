# Greate Places

A new Flutter project.

### Aufgabe:
Schreiben Sie eine App namens Greate Places.

Die App soll eine Liste mit Orten anzeigen, welche dir gefallen. Sobald das Item angeklickt wird, erscheint ein DetailScreen.
Durch einen "+"-Icon kann ein weiteres Item hinzugefügt werden. Es soll auch möglich sein durch die Kamera ein Foto aufzunehmen, 
welches dann in der App angezeigt wird.


#### Ziele
 - Verschiedene Widges (Scaffold, TextButton, ListView, etc.)
 - Navigation zwischen den Screens
 - Die Möglichkeiten von anklickbaren Objekten (onTap, onPressed, etc.)
 - Models anlegen
 - State Managment
 - Umgang mit Maps
 - Google Map benutzen
 - SQLite-Datenbank verwenden




  

- Eine DummyData Datei einlegen mit Werten
- Fonts einbinden und Image
- Menu erstellen (AppBar, TabBar, BottomNavigationBar)
- Entfernen von Items aus der Liste durch Sliver
- Filtern nach bestimmten Kriterien
- Items ins Favorite einfügen
- Einsatz von ChangeNotifierProvider/ChangeNotifierProxyProvider/Providers
- Gezielte Anwendung von Consumern
- Forms
- Bei Fehlermeldungen den AlertDialog oder Snakbar Einsatz
- Firebase verwenden durch die REST-Schnittstelle (Token, userID, etc.)
- Future für Asynchronität
- Animationen
