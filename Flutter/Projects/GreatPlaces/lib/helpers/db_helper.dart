import 'package:greate_places/models/place.dart';
import 'package:path/path.dart' as path;
import 'package:sqflite/sqflite.dart';

class DBHelper {
  ///
  static Future<Database> database() async {
    // Öffnet die Datenbank und speichert die Referenz
    return openDatabase(
      // Legen Sie den Pfad zur Datenbank fest.
      // Hinweis: Die Verwendung der Funktion `join` aus dem `path`-Paket ist
      // die beste Methode, um sicherzustellen, dass der Pfad für jede Plattform korrekt aufgebaut ist.
      path.join(await getDatabasesPath(), 'places.db'),
      // Wenn die Datenbank zum ersten Mal erstellt wird, erstellen Sie eine Tabelle zum Speichern von Orten.
      onCreate: (db, version) {
        // Durchführung der Anweisung CREATE TABLE
        return db.execute(
            'CREATE TABLE places(id TEXT PRIMARY KEY, title TEXT, loc_lat REAL, loc_lng REAL, address TEXT, image TEXT)');
      },
      // Die Version festgelegt. Dies führt die Funktion onCreate aus und
      // stellt einen Pfad zur Durchführung von Datenbank-Upgrades und Downgrades.
      version: 1,
    );
  }

  ///
  static Future<void> insert(String table, Map<String, Object> data) async {
    // Einen Verweis auf die Datenbank holen.
    final database = await DBHelper.database();
    // Hinzufügen eines Ortes in die Tabelle. ConflictAlgorithm bestimmt, ob der
    // Ort zwei mal exsistieren soll. In diesem Fall ersetzen wir herherigen Daten.
    await database.insert(table, data,
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  /// Variante 1
  static Future<List<Map<String, dynamic>>> getData(String table) async {
    // Einen Verweis auf die Datenbank holen.
    final database = await DBHelper.database();
    // Abfrage der Tabelle nach allen Orten
    return await database.query(table);
  }

  /// Variante 2
  static Future<List<Place>> getData2(String table) async {
    // Einen Verweis auf die Datenbank holen.
    final database = await DBHelper.database();
    // Abfrage der Tabelle nach allen Orten
    final List<Map<String, dynamic>> maps = await database.query(table);
    // Umwandlung der List<Map<String, dynamic> in eine List<Place>.
    return List.generate(maps.length, (index) {
      return Place(
        id: maps[index]['id'],
        location: maps[index]['location'],
        title: maps[index]['title'],
        image: maps[index]['image'],
      );
    });
  }
}
