import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:greate_places/helpers/location_helper.dart';
import 'package:greate_places/models/place.dart';
import 'package:greate_places/screens/map_screen.dart';
import 'package:location/location.dart';

class LocationInput extends StatefulWidget {
  final Function onSelectPlace;

  const LocationInput({
    Key? key,
    required this.onSelectPlace,
  }) : super(key: key);

  @override
  _LocationInputState createState() => _LocationInputState();
}

class _LocationInputState extends State<LocationInput> {
  String? _previewImageUrl;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
            height: 170,
            width: double.infinity,
            alignment: Alignment.center,
            decoration:
                BoxDecoration(border: Border.all(width: 1, color: Colors.grey)),
            child: _previewImageUrl != null
                ? Image.network(
                    _previewImageUrl ?? "",
                    fit: BoxFit.cover,
                    width: double.infinity,
                  )
                : const Text("No location choosen",
                    textAlign: TextAlign.center)),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              child: const Text("Current Location"),
              onPressed: _getCurrentUserLocation,
            ),
            TextButton(
              child: const Text("Select on Map"),
              onPressed: _selectOnMap,
            ),
          ],
        )
      ],
    );
  }

  Future<void> _getCurrentUserLocation() async {
    try {
      final locData = await Location().getLocation();
      final latitude = locData.latitude;
      final longitude = locData.longitude;

      if (latitude != null && longitude != null) {
        setState(() {
          _previewImageUrl =
              LocationHelper.generateLocationPreviewImage(latitude, longitude);
        });

        _showPreview(latitude, longitude);
        widget.onSelectPlace(locData.latitude, locData.longitude);
      }
    } catch(error) {
      return;
    }
  }

  Future<void> _selectOnMap() async {
    final LatLng? selectedLocation = await Navigator.of(context).push(
      MaterialPageRoute(
        fullscreenDialog: true,
        builder: (context) => const MapScreen(isSelecting: true),
      ),
    );

    if (selectedLocation == null) return;

    _showPreview(selectedLocation.latitude, selectedLocation.longitude);
    widget.onSelectPlace(selectedLocation.latitude, selectedLocation.longitude);
  }

  Future<void> _showPreview(double latitude, double longitude) async {
    final String? mapImageUrl = LocationHelper.generateLocationPreviewImage(latitude, longitude);

    if (mapImageUrl != null) {
      setState(() {
        _previewImageUrl = mapImageUrl;
      });
    }
  }
}
