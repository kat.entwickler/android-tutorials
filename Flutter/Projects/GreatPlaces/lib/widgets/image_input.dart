import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ImageInput extends StatefulWidget {
  final Function onSelectImage;

  const ImageInput({
    Key? key,
    required this.onSelectImage,
  }) : super(key: key);

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File? _storedImage;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          height: 100,
          width: 150,
          decoration: BoxDecoration(
            border: Border.all(
              width: 1,
              color: Colors.grey,
            ),
          ),
          child: imageOrText(_storedImage),
          alignment: Alignment.center,
        ),
        const SizedBox(
          height: 10,
        ),
        Expanded(
          child: TextButton.icon(
            icon: const Icon(Icons.camera),
            label: const Text("Take Picture"),
            onPressed: _takePicture,
          ),
        ),
      ],
    );
  }

  Widget imageOrText(File? file) {
    if (file != null) {
      return Image.file(
        file,
        fit: BoxFit.cover,
        width: double.infinity,
      );
    } else {
      return const Text("No image taken");
    }
  }

  Future<void> _takePicture() async {
    final imageFile = await ImagePicker().pickImage(
      source: ImageSource.camera,
      maxWidth: 600,
    );

    setState(() {
      if(imageFile != null) {
        _storedImage = File(imageFile.path);
        if(_storedImage != null) {
          widget.onSelectImage(_storedImage);
        }
      }
    });
  }
}
