import 'package:flutter/material.dart';
import 'package:greate_places/screens/add_places_screen.dart';
import 'package:greate_places/screens/place_detail_screen.dart';
import 'package:greate_places/screens/places_list_screen.dart';
import 'package:greate_places/providers/great_places.dart';
import 'package:provider/provider.dart';

import 'core/constants.dart';

void main() {
  // Vermeiden Sie Fehler, die durch ein Flutter-Upgrade verursacht werden.
  // Der Import von 'package:flutter/widgets.dart' ist erforderlich.
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider.value(
      value: GreatPlaces(),
      child: MaterialApp(
        title: 'Great Places',
        theme: ThemeData(
          primarySwatch: Colors.indigo,
            colorScheme: ThemeData.light().colorScheme.copyWith(
              secondary: Colors.amber,
            ),
        ),
        home: const PlacesListScreen(),
        routes: {
          Routes.addPlace: (context) => const AddPlacesScreen(),
          Routes.addPlaceDetail: (context) => const PlaceDetailScreen(),
        },
      ),
    );
  }
}
