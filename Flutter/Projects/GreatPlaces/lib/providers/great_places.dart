import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:greate_places/helpers/db_helper.dart';
import 'package:greate_places/helpers/location_helper.dart';
import 'package:greate_places/models/place.dart';

class GreatPlaces with ChangeNotifier {
  List<Place> _items = [];

  List<Place> get items => [..._items];

  void addPlaces(String title, File image, PlaceLocation pickedLocation) async {
    final address = await LocationHelper.getPlaceAddress(
        pickedLocation.latitude, pickedLocation.longitude);
    final updatedLocation = PlaceLocation(
      latitude: pickedLocation.latitude,
      longitude: pickedLocation.longitude,
      address: address,
    );
    final newPlace = Place(
      id: DateTime.now().toString(),
      location: updatedLocation,
      title: title,
      image: image,
    );

    _items.add(newPlace);
    notifyListeners();

    DBHelper.insert('places', {
      'id': newPlace.id,
      'title': newPlace.title,
      'loc_lat': newPlace.location.latitude,
      'loc_lng': newPlace.location.longitude,
      'address': newPlace.location.address,
      'image': newPlace.image.path,
    });
  }

  Future<void> fetchAndSetPlaces() async {
    // Daten zu Places aus der Datenbank holen
    final dataList = await DBHelper.getData('places');
    // Die Places aus einer Map in eine Liste speichern
    _items = dataList
        .map(
          (item) => Place(
            id: item['id'],
            location: PlaceLocation(

              latitude: item['loc_lat'],
              longitude: item['loc_lng'],
              address: item['address'],
            ),
            title: item['title'],
            image: File(item['image']),
          ),
        )
        .toList();

    notifyListeners();
  }

  Place findById(String id) => items.firstWhere((place) => place.id == id);
}
