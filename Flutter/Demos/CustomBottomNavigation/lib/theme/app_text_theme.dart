import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class AppTextTheme {
  static const TextStyle headline1 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w300, fontSize: 50);
  static const TextStyle headline2 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w300, fontSize: 38);
  static const TextStyle headline3 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600, fontSize: 28);
  static const TextStyle headline4 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w500, fontSize: 18);
  static const TextStyle headline5 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 12);
  static const TextStyle headline5Letter = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 12, letterSpacing: 1);
  static const TextStyle headline6 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 16);
  static const TextStyle headline6Letter = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 16, letterSpacing: 1.3);

  static const TextStyle body10 = TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w400, fontSize: 10);
  static const TextStyle body12 = TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w400, fontSize: 12);
  static const TextStyle body14 = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w400, fontSize: 14);
  static const TextStyle bodyBold = TextStyle(fontFamily: 'Open Sans', fontWeight: FontWeight.w700, fontSize: 16);

  static const TextStyle button = TextStyle(fontFamily: 'Poppins', fontWeight: FontWeight.w600, fontSize: 16);

  static const TextStyle appleSignIn = TextStyle(fontFamily: 'SF Pro Display', fontWeight: FontWeight.w600, fontSize: 19, letterSpacing: 0.36);
  static const TextStyle googleSignIn = TextStyle(fontFamily: 'Roboto', fontWeight: FontWeight.w500, fontSize: 17);
}
