import 'dart:io';
import 'package:flutter/material.dart';
import 'app_colors.dart';
import 'app_text_theme.dart';

class AppTheme {
  static ThemeData get lightTheme {
    return ThemeData.light().copyWith(
      primaryColor: AppColors.neon,
      //TODO scaffoldBackgroundColor: Colors.black,
      cardColor: AppColors.cardColor,
      splashFactory:
          Platform.isIOS ? NoSplash.splashFactory : InkRipple.splashFactory,
      //highlightColor: AppColors.transparent,
      dividerColor: AppColors.dividerColor,
      //hintColor: AppColors.hintColor,
      tabBarTheme: const TabBarTheme(
        indicator: UnderlineTabIndicator(
          borderSide: BorderSide(
            width: 2,
            color: AppColors.neon,
          ),
        ),
        labelColor: AppColors.neon,
        unselectedLabelColor: AppColors.black1,
      ),
      bottomNavigationBarTheme: const BottomNavigationBarThemeData(
        backgroundColor: AppColors.navBarBackground,
        selectedItemColor: AppColors.navBarSelectedItem,
        unselectedItemColor: AppColors.navBarUnselectedItem,
        //selectedLabelStyle: AppColors.headline5,
        //unselectedLabelStyle: AppColors.headline5
      ),
      textButtonTheme: TextButtonThemeData(
        style: TextButton.styleFrom(
            primary: Colors.black,
            textStyle: AppTextTheme.headline6,
            padding: EdgeInsets.zero),
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        backgroundColor: AppColors.neon,
        foregroundColor: Colors.black,
      ),
    );
  }

  static ThemeData get darkTheme {
    return ThemeData.dark().copyWith(
        primaryColor: AppColors.neonDark,
        scaffoldBackgroundColor: Colors.black,
        canvasColor: AppColors.black1Dark,
        cardColor: AppColors.cardColorDark,
        splashFactory:
            Platform.isIOS ? NoSplash.splashFactory : InkRipple.splashFactory,
        highlightColor: Colors.transparent,
        dividerColor: AppColors.dividerColorDark,
        //hintColor: AppColors.hintColorDark,
        tabBarTheme: const TabBarTheme(
            indicator: UnderlineTabIndicator(
              borderSide: BorderSide(
                width: 2,
                color: AppColors.neonDark,
              ),
            ),
            labelColor: AppColors.neonDark,
            unselectedLabelColor: Colors.white),
        bottomNavigationBarTheme: const BottomNavigationBarThemeData(
          backgroundColor: AppColors.navBarBackgroundDark,
          selectedItemColor: AppColors.navBarSelectedItemDark,
          unselectedItemColor: AppColors.navBarUnselectedItemDark,
          selectedLabelStyle: AppTextTheme.headline5,
          unselectedLabelStyle: AppTextTheme.headline5,
        ),
        textButtonTheme: TextButtonThemeData(
          style: TextButton.styleFrom(
              primary: Colors.white,
              textStyle: AppTextTheme.headline6,
              padding: EdgeInsets.zero),
        ),
        floatingActionButtonTheme: const FloatingActionButtonThemeData(
            backgroundColor: AppColors.neonDark,
            foregroundColor: Colors.black));
  }
}
