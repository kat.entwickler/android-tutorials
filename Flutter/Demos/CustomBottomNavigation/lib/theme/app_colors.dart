import 'package:flutter/material.dart';


class AppColors {

  static const Color validGreen = Color(0xFF4DB679);
  static const Color errorRed = Color(0xFF960A00);

  // LightMode
  // Styleguide
  static const Color neon = Color(0xFF31CDBD);
  static const Color neonHover = Color(0xFF1EBDA8);
  static const Color blue = Color(0xFF3C6896);
  static const Color red = Color(0xFFEE334E);
  static const Color grey1 = Color(0xFFF8F9FA);
  static const Color grey2 = Color(0xFFECECEC);
  static const Color grey3 = Color(0xFFACB5BD);
  static const Color grey4 = Color(0xFF666666);
  static const Color grey5 = Color(0xFF979797);
  static const Color black1 = Color(0xFF222222);
  static const Color black2 = Color(0xFF000000);

  static const Color cardColor = Color(0xFFE9F7F6);
  static Color dividerColor = const Color(0xFF3C3C43).withOpacity(0.3);

  static const Color _gradientRed = Color(0xFFEE334E);
  static const Color _gradientBlue = Color(0xFF3584A7);
  static const Color _gradientNeon = Color(0xFF9C4A55);

  static const gradientBlue = LinearGradient(
    begin: Alignment.topRight,
    end: Alignment.bottomLeft,
    colors: <Color>[_gradientRed, _gradientBlue, _gradientNeon],
  );

  // ThemeColors
  static const Color navBarBackground = Color(0xFFFAFAFA);
  static const Color navBarSelectedItem = blue;
  static const Color navBarUnselectedItem = Color(0xFF333333);


  // DarkMode
  // Styleguide
  static const Color neonDark = Color(0xFF31CDBD);
  static const Color neonHoverDark = Color(0xFF1EBDA8);
  static const Color blueDark = Color(0xFF3C6896);
  static const Color redDark = Color(0xFFEE334E);
  static const Color grey1Dark = Color(0xFFF8F9FA);
  static const Color grey2Dark = Color(0xFFDDE2E5);
  static const Color grey3Dark = Color(0xFFACB5BD);
  static const Color grey4Dark = Color(0xFF666666);
  static const Color grey5Dark = Color(0xFF979797);
  static const Color black1Dark = Color(0xFF222222);
  static const Color black2Dark = Color(0xFF000000);

  static const Color cardColorDark = Color(0xFF262629);
  static Color dividerColorDark = const Color(0xFFFFFFFF).withOpacity(0.3);

  static const Color _gradientGreen = Color(0xFF3E6F69);
  static const Color _gradientBlack = Color(0xFF222222);

  static const gradientBlueDark = LinearGradient(
    begin: Alignment.bottomLeft,
    end: Alignment.topRight,
    colors: <Color>[_gradientGreen, _gradientBlack],
  );

  // ThemeColors
  static const Color navBarBackgroundDark = Color(0xFF161616);
  static const Color navBarSelectedItemDark = neonDark;
  static const Color navBarUnselectedItemDark = grey3Dark;
}
