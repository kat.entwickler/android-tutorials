import 'package:custom_bottom_navigation/presentation/routes/route_generator.dart';
import 'package:flutter/material.dart';
import 'core/constants.dart';


void main() {
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<MyApp> {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Flutter Demo',
      initialRoute: Routes.main,
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}
