import 'package:custom_bottom_navigation/application/bloc/navigation/navigation_bloc.dart';
import 'package:custom_bottom_navigation/theme/app_colors.dart';
import 'package:custom_bottom_navigation/theme/app_text_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'constants.dart';

class CustomAppBar extends StatelessWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var brightness = MediaQuery.of(context).platformBrightness;
    bool darkModeOn = brightness == Brightness.dark;
    final bloc = BlocProvider.of<NavigationBloc>(context);

    return Container(
      decoration: BoxDecoration(
        gradient:
            darkModeOn ? AppColors.gradientBlueDark : AppColors.gradientBlue,
        borderRadius: const BorderRadius.only(
          bottomLeft: Radius.circular(20),
          bottomRight: Radius.circular(20),
        ),
      ),
      child: SafeArea(
        child: Container(
          padding: const EdgeInsets.fromLTRB(37, 0, 37, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Transform.translate(
                offset: const Offset(0.0, 20.0),
                child: BlocBuilder<NavigationBloc, NavigationState>(
                  bloc: bloc,
                  builder: (context, state) {
                    final forward =
                        bloc.previousTab.index < bloc.currentTab.index;
                    final title = titles.length > bloc.currentTab.index
                        ? titles[bloc.currentTab.index]
                        : "";

                    return _buildAnimatedTitle(forward, title.toUpperCase());
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  // Explanation: https://medium.com/flutter-community/what-do-you-know-about-aniamtedswitcher-53cc3a4bebb8
  AnimatedSwitcher _buildAnimatedTitle(bool forward, String title) {
    return AnimatedSwitcher(
      duration: const Duration(milliseconds: 200),
      transitionBuilder: (Widget child, Animation<double> animation) {
        final inAnimation = Tween<Offset>(
                begin: Offset(forward ? 1.0 : -1.0, 0.0), end: Offset.zero)
            .animate(animation);
        final outAnimation = Tween<Offset>(
                begin: Offset(forward ? -1.0 : 1.0, 0.0), end: Offset.zero)
            .animate(animation);

        return FadeTransition(
            opacity: animation,
            child: SlideTransition(
              position:
                  (child.key == ValueKey(title)) ? inAnimation : outAnimation,
              child: child,
            ));
      },
      child: Align(
        alignment: Alignment.center,
        key: ValueKey<String>(title),
        child: Text(
          title,
          style: AppTextTheme.headline3.copyWith(color: Colors.white),
        ),
      ),
    );
  }
}
