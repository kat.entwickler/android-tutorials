import 'package:custom_bottom_navigation/presentation/favourite/favourite_screen.dart';
import 'package:custom_bottom_navigation/presentation/home/home_screen.dart';
import 'package:custom_bottom_navigation/presentation/selection/choice_star_screen.dart';
import 'package:custom_bottom_navigation/presentation/selection/overview_screen.dart';
import 'package:custom_bottom_navigation/presentation/selection/selection_star_screen.dart';
import 'package:custom_bottom_navigation/presentation/team/team_screen.dart';
import 'package:custom_bottom_navigation/presentation/profile/profile_screen.dart';
import 'package:flutter/material.dart';

import 'constants.dart';

class TabNavigator extends StatelessWidget {
  final GlobalKey<NavigatorState> navigatorKey;
  final String initialRoute;

  const TabNavigator(
      {Key? key, required this.navigatorKey, required this.initialRoute})
      : super(key: key);

  Map<String, WidgetBuilder> _routeBuilders(BuildContext context) {
    return {
      TabNavigatorRoutes.home: (context) => const HomeScreen(),
      TabNavigatorRoutes.team: (context) => const TeamScreen(),
      TabNavigatorRoutes.selection: (context) => OverviewScreen(
            onPlusTapped: (route) => _push(context, route: route),
          ),
      TabNavigatorRoutes.selectionStar: (context) => ChoiceStarScreen(
            onPlayerTapped: (route) => _push(context, route: route),
          ),
      TabNavigatorRoutes.addStar: (context) => const SelectionStarScreen(),
      TabNavigatorRoutes.favourite: (context) => const FavouriteScreen(),
      TabNavigatorRoutes.profile: (context) => const ProfileScreen(),
    };
  }

  void _push(BuildContext context, {required String route}) {
    var routeBuilders = _routeBuilders(context);
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => routeBuilders[route]!(context),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var routeBuilders = _routeBuilders(context);

    return Navigator(
      key: navigatorKey,
      initialRoute: initialRoute,
      onGenerateRoute: (routeSettings) {
        if (routeBuilders.containsKey(routeSettings.name)) {
          return MaterialPageRoute(
            builder: (context) => routeBuilders[routeSettings.name]!(context),
          );
        }
      },
    );
  }
}
