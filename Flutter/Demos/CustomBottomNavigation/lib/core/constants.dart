import 'package:custom_bottom_navigation/infrastructure/model/person.dart';
import 'package:flutter/cupertino.dart';

/// GlobalKey for app navigator.
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

/// Routing constants used within the app
class Routes {
  static const String initial = '/';
  static const String main = '/main';
  static const String transfer = '/transfer';
  static const String transferDetails = '/transferDetails';
}

class TabNavigatorRoutes {
  static const String home = '/home';
  static const String team = '/team';
  static const String selection = '/selection';
  static const String selectionStar = '/selectionStar';
  static const String addStar = '/addStar';
  static const String favourite = '/favourite';
  static const String profile = '/profile';
}

List<String> titles = [
  "Home",
  "Team",
  "Selection",
  "Favourite",
  "Profile",
];

List<Person> person = [
  Person(
    id: 1,
    name: 'Will Smith',
    size: "1,88 m",
    birthday: "25.09.1968",
    image:
        "https://www.jolie.de/sites/default/files/styles/image870w/public/images/2017/07/31/316385_9_die-schoensten-promi-maenner-platz-6-will-smith.webp?itok=mxqaLjE4",
  ),
  Person(
    id: 2,
    name: 'David Beckham',
    size: "01,80 m",
    birthday: "02.05.1975",
    image:
        "https://www.jolie.de/sites/default/files/styles/image870w/public/images/2017/07/31/316385_13_die-schoensten-promi-maenner-platz-5-david-beckham.webp?itok=mdDRlyj2",
  ),
  Person(
    id: 3,
    name: 'Brad Pitt',
    size: "1,80 m",
    birthday: "18.12.1963",
    image:
        "https://www.jolie.de/sites/default/files/styles/image870w/public/images/2017/07/31/316385_17_die-schoensten-promi-maenner-platz-3-brad-pitt.webp?itok=moipdv41",
  ),
];
