import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../application/bloc/navigation/navigation_bloc.dart';
import '../application/bloc/navigation/navigation_tab_item.dart';
import 'constants.dart';

class CustomBottomNavigationBar extends StatelessWidget {
  const CustomBottomNavigationBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = BlocProvider.of<NavigationBloc>(context);

    return BlocBuilder<NavigationBloc, NavigationState>(
      bloc: bloc,
      builder: (context, state) {
        return BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: bloc.currentTab.index,
          onTap: (index) =>
              bloc.add(PageTapped(tabItem: NavigationTabItem.values[index])),
          items: [
            BottomNavigationBarItem(
              label: titles[0],
              icon: _buildIcon(CupertinoIcons.sportscourt),
            ),
            BottomNavigationBarItem(
              label: titles[1],
              icon: _buildIcon(CupertinoIcons.person_2),
            ),
            BottomNavigationBarItem(
              label: titles[2],
              icon: _buildIcon(CupertinoIcons.arrow_2_squarepath),
            ),
            BottomNavigationBarItem(
              label: titles[3],
              icon: _buildIcon(CupertinoIcons.star),
            ),
            BottomNavigationBarItem(
              label: titles[4],
              icon: _buildIcon(CupertinoIcons.person),
            ),
          ],
        );
      },
    );
  }

  Padding _buildIcon(IconData iconData) => Padding(
      padding: const EdgeInsets.only(top: 5, bottom: 5), child: Icon(iconData));
}
