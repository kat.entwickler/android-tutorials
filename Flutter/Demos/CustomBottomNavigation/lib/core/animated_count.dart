import 'package:custom_bottom_navigation/theme/app_text_theme.dart';
import 'package:custom_bottom_navigation/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';


class AnimatedCount extends ImplicitlyAnimatedWidget {
  const AnimatedCount(
      {Key? key,
      required this.count,
      Duration duration = const Duration(milliseconds: 600),
      Curve curve = Curves.fastOutSlowIn,
      this.isCurrency = false
      }) : super(duration: duration, curve: curve, key: key);

  final num count;
  final bool isCurrency;

  @override
  ImplicitlyAnimatedWidgetState<ImplicitlyAnimatedWidget> createState() {
    return _AnimatedCountState();
  }
}

class _AnimatedCountState extends AnimatedWidgetBaseState<AnimatedCount> {
  late IntTween _intCount;
  Tween<double> _doubleCount = Tween<double>();
  late final NumberFormat _formatter;

  @override
  void initState() {
    _intCount = IntTween(begin: widget.count.toInt(), end: 1);
    _formatter =
        widget.isCurrency ? getCurrencyFormatter() : getDecimalFormatter();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    HapticFeedback.selectionClick();

    return FittedBox(
      fit: BoxFit.fitWidth,
      child: widget.count is int
          ? Text(_formatter.format(_intCount.evaluate(animation)),
              style: AppTextTheme.headline4.copyWith(color: Colors.white),
              maxLines: 1)
          : Text(_doubleCount.evaluate(animation).toStringAsFixed(1),
              style: AppTextTheme.headline4.copyWith(color: Colors.white))
    );
  }

  @override
  void forEachTween(TweenVisitor<dynamic> visitor) {
    if (widget.count is int) {
      _intCount = visitor(
        _intCount,
        widget.count,
        (dynamic value) => IntTween(begin: value)
      ) as IntTween;
    } else {
      _doubleCount = visitor(
        _doubleCount,
        widget.count,
        (dynamic value) => Tween<double>(begin: value)
      ) as Tween<double>;
    }
  }
}
