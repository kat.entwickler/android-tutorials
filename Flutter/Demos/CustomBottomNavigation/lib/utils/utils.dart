import 'package:intl/intl.dart' as intl;


getCurrencyFormatter() {
  return intl.NumberFormat.simpleCurrency(locale: 'eu', decimalDigits: 0);
}

getDecimalFormatter() {
  return intl.NumberFormat.decimalPattern('eu');
}
