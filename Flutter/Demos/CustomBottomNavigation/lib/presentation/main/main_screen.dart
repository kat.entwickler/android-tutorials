import 'package:custom_bottom_navigation/application/bloc/navigation/navigation_bloc.dart';
import 'package:custom_bottom_navigation/application/bloc/navigation/navigation_tab_item.dart';
import 'package:custom_bottom_navigation/core/custom_app_bar.dart';
import 'package:custom_bottom_navigation/core/tab_navigator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../core/custom_bottom_navigation_bar.dart';

class MainScreen extends StatelessWidget {
  MainScreen({Key? key}) : super(key: key);

  final navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    final navigationBloc = BlocProvider.of<NavigationBloc>(context);

    return WillPopScope(
      onWillPop: () async {
        final isFirstRouteInCurrentTab = await navigationBloc
            .navigatorKeys[navigationBloc.currentTab]!.currentState!
            .maybePop();
        if (isFirstRouteInCurrentTab) {
          if (navigationBloc.currentTab != NavigationTabItem.home) {
            return false;
          }
        }
        return isFirstRouteInCurrentTab;
      },
      child: Scaffold(
        appBar: const PreferredSize(
          preferredSize: Size(double.infinity, 75),
          child: CustomAppBar(),
        ),
        extendBodyBehindAppBar: true,
        body: BlocBuilder<NavigationBloc, NavigationState>(
          builder: (context, state) {
            if (navigationBloc.previousTab == navigationBloc.currentTab) {
              navigationBloc
                  .navigatorKeys[navigationBloc.currentTab]!.currentState
                  ?.popUntil((route) => route.isFirst);
            }
            return Stack(
              children: <Widget>[
                _buildOffstageNavigator(
                    navigationBloc, NavigationTabItem.home),
                _buildOffstageNavigator(navigationBloc, NavigationTabItem.team),
                _buildOffstageNavigator(
                    navigationBloc, NavigationTabItem.selection),
                _buildOffstageNavigator(
                    navigationBloc, NavigationTabItem.favourite),
                _buildOffstageNavigator(
                    navigationBloc, NavigationTabItem.profile),
              ],
            );
          },
        ),
        bottomNavigationBar: const CustomBottomNavigationBar(),
      ),
    );
  }

  Widget _buildOffstageNavigator(
      NavigationBloc navigationBloc, NavigationTabItem tabItem) {
    return Offstage(
      offstage: navigationBloc.currentTab != tabItem,
      child: TabNavigator(
        navigatorKey: navigationBloc.navigatorKeys[tabItem]!,
        initialRoute: tabItem.tabNavigatorRoute(),
      ),
    );
  }
}
