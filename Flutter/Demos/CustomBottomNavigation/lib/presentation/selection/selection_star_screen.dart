import 'package:custom_bottom_navigation/application/bloc/selection/selection_bloc.dart';
import 'package:custom_bottom_navigation/core/constants.dart';
import 'package:custom_bottom_navigation/presentation/core/headline.dart';
import 'package:custom_bottom_navigation/presentation/core/primary_button.dart';
import 'package:custom_bottom_navigation/presentation/selection/widgets/person_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SelectionStarScreen extends StatelessWidget {
  const SelectionStarScreen({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const horizontalPadding = EdgeInsets.symmetric(horizontal: 20);
    const verticalPadding1 = SizedBox(height: 10);
    const verticalPadding2 = SizedBox(height: 20);
    return Container(
      color: Theme
          .of(context)
          .scaffoldBackgroundColor,
      child: Padding(
        padding: horizontalPadding,
        child: ListView(
          clipBehavior: Clip.none,
          children: [
            verticalPadding2,
            const Headline(name: "Sie haben gewählt"),
            const SizedBox(height: 24),
            BlocBuilder<SelectionBloc, SelectionState>(
              builder: (context, state) {
                print(state);
                if (state is CurrentPersonId) {
                  print("CurrentPersonId: ${state.personId}");
                  return _buildPlaceholderItem(index: state.personId);
                }
                return const Placeholder();
              },
            ),
            const SizedBox(height: 28),
            PrimaryButton(
              title: "Hinzufügen",
              onPressed: () =>
                  Navigator.popUntil(context, (route) => route.isFirst),
            ),
            verticalPadding1,
            const Text(
                "Dies ist ein Infotext, der noch formuliert werden muss."),
            const SizedBox(height: 50),
          ],
        ),
      ),
    );
  }

  PersonItem _buildPlaceholderItem({required int index}) {
    print("Star: ${index}");
    return PersonItem(person: person[index]);
  }
}
