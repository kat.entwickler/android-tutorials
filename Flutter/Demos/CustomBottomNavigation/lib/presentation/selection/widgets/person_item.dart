import 'package:custom_bottom_navigation/infrastructure/model/person.dart';
import 'package:custom_bottom_navigation/presentation/core/custom_network_image.dart';
import 'package:custom_bottom_navigation/presentation/core/name_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PersonItem extends StatelessWidget {
  final Person person;
  const PersonItem({Key? key, required this.person}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return SizedBox(
        height: 100,
        width: double.infinity,
        child: Card(
            child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 15),
                        child: CustomNetworkImage(imageUrl: person.image),
                      ),
                      const SizedBox(width: 20),
                      Expanded(
                          child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                NameField(name: person.name)
                              ]
                          )
                      )
                    ]
                )
            )
        )
    );
  }
}
