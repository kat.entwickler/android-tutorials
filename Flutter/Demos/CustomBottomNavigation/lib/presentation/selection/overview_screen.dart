import 'package:custom_bottom_navigation/core/constants.dart';
import 'package:custom_bottom_navigation/presentation/core/headline.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OverviewScreen extends StatelessWidget {
  final ValueChanged<String>? onPlusTapped;

  const OverviewScreen({Key? key, this.onPlusTapped}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: const Center(
        child: Headline(name: "Selection"),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add, size: 30),
        onPressed: () => onPlusTapped?.call(TabNavigatorRoutes.selectionStar),
      ),
    );
  }
}
