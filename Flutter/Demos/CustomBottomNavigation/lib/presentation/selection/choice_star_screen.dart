import 'package:custom_bottom_navigation/application/bloc/selection/selection_bloc.dart';
import 'package:custom_bottom_navigation/presentation/core/headline.dart';
import 'package:custom_bottom_navigation/presentation/selection/widgets/person_item.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../core/constants.dart';

class ChoiceStarScreen extends StatelessWidget {
  final ValueChanged<String>? onPlayerTapped;

  const ChoiceStarScreen({Key? key, required this.onPlayerTapped}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final selectionBloc = BlocProvider.of<SelectionBloc>(context);
    const horizontalPadding = EdgeInsets.symmetric(horizontal: 20);
    const verticalSpacing = SizedBox(height: 10);

    return Container(
      padding: horizontalPadding,
      color: Theme.of(context).scaffoldBackgroundColor,
      child: ListView.separated(
        itemCount: person.length + 1,
        separatorBuilder: (BuildContext context, int index) => verticalSpacing,
        itemBuilder: (BuildContext context, int index) {
          if (index == 0) {
            return const Padding(
              padding: EdgeInsets.only(top: 20, bottom: 14),
              child: Headline(name: "Treffen Sie ihre Auswahl"),
            );
          }

          return GestureDetector(
            onTap: () {
              selectionBloc.add(StarContentEvent(currentPersonId: index - 1));
              onPlayerTapped?.call(TabNavigatorRoutes.addStar);
              print("Index: ${index - 1}");
            },
            child: _buildPlaceholderItem(index: index - 1),
          );
        },
      ),
    );
  }

  PersonItem _buildPlaceholderItem({required int index}) {
    return PersonItem(person: person[index]);
  }
}
