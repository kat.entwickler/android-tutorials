import 'package:custom_bottom_navigation/theme/app_text_theme.dart';
import 'package:flutter/cupertino.dart';



class NameField extends StatelessWidget {
  final String name;
  const NameField({Key? key, required this.name}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return FittedBox(
      fit: BoxFit.scaleDown,
      child: Text(name, style: AppTextTheme.headline6)
    );
  }
}
