import 'package:custom_bottom_navigation/theme/app_text_theme.dart';
import 'package:flutter/material.dart';


class PositionLabel extends StatelessWidget {
  final String position;
  const PositionLabel({Key? key, required this.position}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 4, vertical: 2),
      color: Theme.of(context).primaryColor,
      child: Text(position, style: AppTextTheme.headline5)
    );
  }
}
