import 'package:custom_bottom_navigation/theme/app_colors.dart';
import 'package:custom_bottom_navigation/theme/app_text_theme.dart';
import 'package:flutter/material.dart';


class PrimaryButton extends StatelessWidget {
  final String title;
  final VoidCallback? onPressed;

  const PrimaryButton({Key? key, this.title = '', this.onPressed,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      child: Text(title, style: AppTextTheme.button),
      color: AppColors.neon,
      hoverColor: const Color(0xFF1EBDA8),
      highlightColor: const Color(0xFF1EBDA8),
      textColor: Colors.black,
      disabledTextColor: const Color(0xFFACB5BD),
      disabledColor: const Color(0xFFDDE2E5),
      elevation: 0,
      highlightElevation: 0,
      focusElevation: 0,
      disabledElevation: 0,
      hoverElevation: 0,
      height: 48,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      minWidth: double.infinity,
    );
  }
}