import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';


class CustomNetworkImage extends StatelessWidget {
  final String imageUrl;
  final double? height;
  const CustomNetworkImage({Key? key, required this.imageUrl, this.height}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: imageUrl,
      fadeInDuration: const Duration(milliseconds: 300),
      placeholder: (context, url) => const CupertinoActivityIndicator(animating: true, radius: 10),
      errorWidget: (context, url, error) => SvgPicture.asset('assets/images/player_placeholder.svg'),
      height: height,
    );
  }
}
