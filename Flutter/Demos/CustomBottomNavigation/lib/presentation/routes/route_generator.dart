import 'package:custom_bottom_navigation/application/bloc/selection/selection_bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../application/bloc/navigation/navigation_bloc.dart';
import '../../core/constants.dart';
import '../main/main_screen.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case Routes.main:
        return MaterialPageRoute(
            builder: (_) => MultiBlocProvider(
                  providers: [
                    BlocProvider(
                      create: (context) => NavigationBloc(),
                    ),
                    BlocProvider(
                      create: (context) => SelectionBloc(),
                    )
                  ],
                  child: MainScreen(),
                ));

      default:
        // If there is no such named route in the switch statement.
        return _errorRoute();
    }
  }

  // TODO: Build Generic ErrorScreen.
  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
          appBar: AppBar(title: const Text('Error')),
          body: const Center(child: Text('ERROR')));
    });
  }
}
