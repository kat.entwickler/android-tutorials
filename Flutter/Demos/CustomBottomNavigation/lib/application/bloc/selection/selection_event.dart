part of 'selection_bloc.dart';

@immutable
abstract class SelectionEvent {}

class StarContentEvent extends SelectionEvent{
  final int currentPersonId;
  StarContentEvent({required this.currentPersonId});
}
