import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'selection_event.dart';
part 'selection_state.dart';

class SelectionBloc extends Bloc<SelectionEvent, SelectionState> {
  var _currentPersonId = 0;

  int get currentPersonId => _currentPersonId;

  SelectionBloc() : super(SelectionInitial()) {
    on<StarContentEvent>((event, emit) {
      print("EventBloc: ${event.currentPersonId}");
      _currentPersonId = event.currentPersonId;
      emit(CurrentPersonId(personId: _currentPersonId));
    });
  }
}
