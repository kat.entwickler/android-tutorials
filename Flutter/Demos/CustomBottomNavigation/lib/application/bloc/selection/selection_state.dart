part of 'selection_bloc.dart';

@immutable
abstract class SelectionState {}

class SelectionInitial extends SelectionState {}

class CurrentPersonId extends SelectionState {
  final int personId;
  CurrentPersonId({required this.personId}) : super();
}
