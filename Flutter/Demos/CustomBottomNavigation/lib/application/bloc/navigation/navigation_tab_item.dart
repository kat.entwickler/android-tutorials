import 'package:flutter/foundation.dart';

import '../../../core/constants.dart';

enum NavigationTabItem { home, team, selection, favourite, profile }

extension NavigationTabItemExtension on NavigationTabItem {
  String get name => describeEnum(this);

  String tabNavigatorRoute() {
    switch (this) {
      case NavigationTabItem.home:
        return TabNavigatorRoutes.home;
      case NavigationTabItem.team:
        return TabNavigatorRoutes.team;
      case NavigationTabItem.selection:
        return TabNavigatorRoutes.selection;
      case NavigationTabItem.favourite:
        return TabNavigatorRoutes.favourite;
      case NavigationTabItem.profile:
        return TabNavigatorRoutes.profile;
    }
  }
}
