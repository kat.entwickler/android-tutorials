part of 'navigation_bloc.dart';

abstract class NavigationEvent {
  const NavigationEvent() : super();
}

class AppStarted extends NavigationEvent {}

class PageTapped extends NavigationEvent {
  final NavigationTabItem tabItem;

  const PageTapped({required this.tabItem}) : super();
}
