import 'package:bloc/bloc.dart';
import 'package:custom_bottom_navigation/application/bloc/navigation/navigation_tab_item.dart';
import 'package:flutter/cupertino.dart';

part 'navigation_event.dart';

part 'navigation_state.dart';

class NavigationBloc extends Bloc<NavigationEvent, NavigationState> {
  var _previousTab = NavigationTabItem.home;
  var _currentTab = NavigationTabItem.home;

  final _navigatorKeys = {
    NavigationTabItem.home: GlobalKey<NavigatorState>(),
    NavigationTabItem.team: GlobalKey<NavigatorState>(),
    NavigationTabItem.selection: GlobalKey<NavigatorState>(),
    NavigationTabItem.favourite: GlobalKey<NavigatorState>(),
    NavigationTabItem.profile: GlobalKey<NavigatorState>(),
  };

  NavigationTabItem get previousTab => _previousTab;

  NavigationTabItem get currentTab => _currentTab;

  Map<NavigationTabItem, GlobalKey<NavigatorState>> get navigatorKeys =>
      _navigatorKeys;

  NavigationBloc() : super(NavigationInitial()) {
    on<NavigationEvent>((event, emit) {
      if (event is AppStarted) {
        _previousTab = NavigationTabItem.home;
        _currentTab = NavigationTabItem.home;
      } else if (event is PageTapped) {
        _previousTab = _currentTab;
        _currentTab = event.tabItem;
      }

      emit(CurrentIndexChanged(
        tabItem: _currentTab,
      ));
    });
  }
}
