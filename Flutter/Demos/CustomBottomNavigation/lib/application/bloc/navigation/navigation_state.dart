part of 'navigation_bloc.dart';

@immutable
abstract class NavigationState {}

class NavigationInitial extends NavigationState {}

class CurrentIndexChanged extends NavigationState {
  final NavigationTabItem tabItem;

  CurrentIndexChanged({required this.tabItem}) : super();
}
