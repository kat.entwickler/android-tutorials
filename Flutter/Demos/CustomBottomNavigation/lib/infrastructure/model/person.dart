class Person {
  final int id;
  final String name;
  final String size;
  final String birthday;
  final String image;

  Person({
    required this.id,
    required this.name,
    required this.size,
    required this.birthday,
    required this.image,
  });
}
