# custom_bottom_navigation

Ein neues Flutter Demo.

## Ziele

Dieses Beispiel soll die Navigation innerhalb eines BottomNavigation und AppBar zeigen. Gleichzeitig soll innerhalb des Bodys eine Seite zu Seite Navigation möglich sein, ohne die App komplett neu zu zeichnen.

Folgende Werkzeuge wurden verwendet:

- BLoC
- GlobalKey
- RouteGenerator
- Enum
- NavigationTabItem
- BottomNavigationBarItem
- Stack
