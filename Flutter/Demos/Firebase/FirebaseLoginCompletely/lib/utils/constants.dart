/// Routing constants used within the app
class Routes {
  static const String initial = '/';
  static const String home = '/home_screen';
  static const String login = '/login_screen';
  static const String register = '/signup_screen';
  static const String resetPassword = '/reset_password_screen';
}
