import 'package:flutter_bloc/flutter_bloc.dart';

import '../enum/auth_status.dart';
import '../repository/auth_repository.dart';

part 'auth_event.dart';

part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository _authRepository;

  AuthBloc({required AuthRepository authRepository})
      : _authRepository = authRepository,
        super(AuthInitial()) {
    on<LoginEvent>((event, emit) async {
      if (event.email != null || event.password != null) {
        AuthStatus status = await _authRepository.login(
          email: event.email!,
          password: event.password!,
        );

        if (AuthStatus.successful == status) {
          add(EmailVerifiedEvent());
        } else {
          emit(LoginStatus(authState: status));
        }
      }
    });

    on<CreateAccountEvent>((event, emit) async {
      if (event.name != null || event.email != null || event.password != null) {
        AuthStatus status = await _authRepository.createAccount(
          name: event.name!,
          email: event.email!,
          password: event.password!,
        );

        emit(CreateAccountStatus(authState: status));
      }
    });

    on<ResetPasswordEvent>((event, emit) async {
      if (event.email != null) {
        AuthStatus status = await _authRepository.resetPassword(email: event.email!);

        emit(ResetPasswordStatus(authState: status));
      }
    });

    on<EmailVerifiedEvent>((event, emit) async {
     AuthStatus status = await _authRepository.isEmailVerified();

     emit(LoginStatus(authState: status));
    });

    on<LogoutEvent>((event, emit) async {
      _authRepository.logout();
    });
  }
}
