part of 'auth_bloc.dart';

abstract class AuthState {}

class AuthInitial extends AuthState {}

class LoginStatus extends AuthState {
  final AuthStatus authState;

  LoginStatus({required this.authState});
}

class CreateAccountStatus extends AuthState {
  final AuthStatus authState;

  CreateAccountStatus({required this.authState});
}

class ResetPasswordStatus extends AuthState {
  final AuthStatus authState;

  ResetPasswordStatus({required this.authState});
}
