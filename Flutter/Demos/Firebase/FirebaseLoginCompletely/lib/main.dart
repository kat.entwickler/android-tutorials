import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_login_completety/enum/auth_status.dart';
import 'package:firebase_login_completety/repository/auth_repository_impl.dart';
import 'package:firebase_login_completety/utils/constants.dart';
import 'package:firebase_login_completety/utils/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/auth_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => AuthBloc(
            authRepository: AuthRepositoryImp(
              firebaseAuth: FirebaseAuth.instance,
              authStatus: AuthStatus.unknown,
            ),
          ),
        ),
      ],
      child: MaterialApp(
        title: 'Firebase Demo',
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: Routes.login,
        onGenerateRoute: MyRouter.generateRoute,
      ),
    );
  }
}
