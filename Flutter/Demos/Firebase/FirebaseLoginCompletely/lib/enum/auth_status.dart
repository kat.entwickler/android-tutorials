enum AuthStatus {
  successful,
  wrongPassword,
  emailAlreadyExists,
  operationNotAllowed,
  invalidEmail,
  weakPassword,
  invalidEmailVerified,
  unknown,
}
