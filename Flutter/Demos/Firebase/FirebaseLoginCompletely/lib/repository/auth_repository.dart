import '../enum/auth_status.dart';

///
abstract class AuthRepository {
  ///
  Future<AuthStatus> createAccount({
    required String name,
    required String email,
    required String password,
  });

  ///
  Future<AuthStatus> login({
    required String email,
    required String password,
  });

  ///
  Future<AuthStatus> resetPassword({required String email});

  ///
  Future<AuthStatus> isEmailVerified();

  ///
  Future<void> logout();
}
