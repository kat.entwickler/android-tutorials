import 'package:firebase_auth/firebase_auth.dart';

import '../enum/auth_status.dart';
import '../failures/auth_exception_handler.dart';
import 'auth_repository.dart';

class AuthRepositoryImp implements AuthRepository {
  final FirebaseAuth _firebaseAuth;
  late AuthStatus _authStatus;

  AuthRepositoryImp({
    required FirebaseAuth firebaseAuth,
    required AuthStatus authStatus,
  })  : _firebaseAuth = firebaseAuth,
        _authStatus = authStatus,
        super();

  @override
  Future<AuthStatus> createAccount({
    required String name,
    required String email,
    required String password,
  }) async {
    try {
      UserCredential newUser = await _firebaseAuth.createUserWithEmailAndPassword(
        email: email,
        password: password,
      );
      _firebaseAuth.currentUser!.updateDisplayName(name);
      newUser.user?.sendEmailVerification();
      _authStatus = AuthStatus.successful;
    } on FirebaseAuthException catch (e) {
      _authStatus = AuthExceptionHandler.handleAuthException(e);
    }
    return _authStatus;
  }

  @override
  Future<AuthStatus> login({
    required String email,
    required String password,
  }) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
      _authStatus = AuthStatus.successful;
    } on FirebaseAuthException catch (e) {
      _authStatus = AuthExceptionHandler.handleAuthException(e);
    }
    return _authStatus;
  }

  @override
  Future<AuthStatus> resetPassword({required String email}) async {
    await _firebaseAuth
        .sendPasswordResetEmail(email: email)
        .then((_) => _authStatus = AuthStatus.successful)
        .catchError((exception) => _authStatus = AuthExceptionHandler.handleAuthException(exception));
    return _authStatus;
  }

  @override
  Future<AuthStatus> isEmailVerified() async {
    User? user = _firebaseAuth.currentUser;
    bool? isEmailVerified = user?.emailVerified;

    if (isEmailVerified != null && !isEmailVerified) {
      _authStatus = AuthStatus.invalidEmailVerified;
      user?.sendEmailVerification();
    } else {
      _authStatus = AuthStatus.successful;
    }
    return _authStatus;
  }

  @override
  Future<void> logout() async {
    await _firebaseAuth.signOut();
  }
}
