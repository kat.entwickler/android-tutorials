import '../enum/auth_status.dart';

class ErrorMessageHandler {
  static String generateErrorMessage(error) {
    String errorMessage;
    switch (error) {
      case AuthStatus.invalidEmail:
        errorMessage = "Your email address appears to be malformed.";
        break;
      case AuthStatus.weakPassword:
        errorMessage = "Your password should be at least 6 characters.";
        break;
      case AuthStatus.wrongPassword:
        errorMessage = "Your email or password is wrong.";
        break;
      case AuthStatus.emailAlreadyExists:
        errorMessage = "The email address is already in use by another account.";
        break;
      case AuthStatus.operationNotAllowed:
        errorMessage = "The given sign-in provider is disabled for this Firebase project. Enable it in the Firebase console, under the sign-in method tab of the Auth section.";
        break;
      case AuthStatus.invalidEmailVerified:
        errorMessage = "The provided value for the emailVerified user property is invalid. It must be a boolean.";
        break;
      default:
        errorMessage = "An error occurred. Please try again later.";
    }
    return errorMessage;
  }
}
