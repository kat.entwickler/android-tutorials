import 'package:firebase_auth/firebase_auth.dart';

import '../enum/auth_status.dart';

class AuthExceptionHandler {
  static handleAuthException(FirebaseAuthException e) {
    AuthStatus status;
    switch (e.code) {
      case "invalid-email":
        status = AuthStatus.invalidEmail;
        break;
      case "weak-password":
        status = AuthStatus.weakPassword;
        break;
      case "wrong-password":
        status = AuthStatus.wrongPassword;
        break;
      case "email-already-in-use":
        status = AuthStatus.emailAlreadyExists;
        break;
      case "operation-not-allowed":
        status = AuthStatus.operationNotAllowed;
        break;
      case "invalid-email-verified":
        status = AuthStatus.invalidEmailVerified;
        break;
      default:
        status = AuthStatus.unknown;
    }
    return status;
  }
}
